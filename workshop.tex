\documentclass[nonacm,sigplan,screen]{acmart}

%\usepackage{mygeometry}
\usepackage{mymath}
\usepackage{mylistings}
\usepackage{myhyperref}
\usepackage{mybibliography}

\usepackage{notations}

\title{Unfolding ML datatype declarations without loops}
\author{Nicolas Chataing, Gabriel Scherer} % ordre alphabétique

\begin{document}

\maketitle

\section{Introduction}
\paragraph{Unboxing a single-constructor  datatype}
ML-family languages support both \emph{type abbreviations}, which
provide a synonym for an existing type, and \emph{datatypes}
(sums/variants and records) that provide a new type (distinct from
previous types) specified by its constructors or fields.

Since 4.06 (June 2016), OCaml additionally supports \emph{unboxed
  datatypes}, which are single-constructor variants or single-field
records that behave like datatypes during type-checking (they are
distinct types) and abbreviations at runtime -- constructor
application or pattern-matching are erased.

\begin{lstlisting}
type an_abbrev = int list
type a_datatype = Short of int | Long of int list
type an_unboxed_type = Short of int [@@unboxed]
\end{lstlisting}
(SML uses an explicit \lstinline{datatype} keyword for datatype
declarations, and Haskell uses \lstinline{data} for datatypes,
\lstinline{type} for abbreviations and \lstinline{newtype} for unboxed
datatypes.)

\paragraph{Unboxing individual constructors within a datatype}
In \href{https://github.com/ocaml/RFCs/pull/14}{ocaml/RFCs\#14} Jeremy
Yallop proposed to allow unboxing a constructor even if the datatype
has other constructors:
\begin{lstlisting}
type partial_unboxing =
| Short of int [@unboxed]
| Long of int list
\end{lstlisting}
This example makes sense because, even if the \lstinline{Short}
constructor is elided at runtime, the representation is unambiguous:
primitive integers \lstinline{int} can always be distinguished from
blocks like \lstinline{Long of int list}.

For example, the pattern-matching clauses
\begin{lstlisting}
| Short n -> ...
| Long li -> ...
\end{lstlisting}
%
would previously test whether the head constructor of the value is
\lstinline{Short} or \lstinline{Long}. With the unboxed annotation,
\lstinline{Short} is not present at runtime, but we can instead check
whether we have a primitive integer or \lstinline{Long}.

In the general case, unboxing a constructor could be allowed whenever
the representation of its parameter is disjoint/distinguishable from
the representation of all the other values of the datatype. If
this disjointness condition does not hold, we will not be able to
implement pattern-matching on this datatype; the unboxing declaration
must be rejected with an error.

\paragraph{Our work} We are working on an implementation of this
proposal to allow individual constructor unboxing. In particular, to
determine whether unboxing a given constructor should be allowed or
rejected, we compute the \emph{head shape} of type expressions and
type declarations, that is some abstract over-approximation of the
set of possible values used to check this disjointness condition.

\paragraph{Unfolding without loops?} To compute the head shape of
a type expression \lstinline{int t}, we need to inspect/unfold the
definition of the datatype \lstinline{'a t}, which in turn requires
computing the head shape of the parameters of its unboxed
constructors. For example, with
%
\begin{lstlisting}
type 'a t =
| Foo of foo  [@unboxed]
| Bar of bar [@unboxed]
\end{lstlisting}
%
the head shape is the union of the shapes of \lstinline{foo} and
\lstinline{bar}, whose computation may in turn require following
(data)type definitions.

In presence of mutually-recursive datatypes, repeated unfolding may
lead to non-termination. For a pathological example:
%
\begin{lstlisting}
type loop = Loop of loop [@unboxed]
\end{lstlisting}
%
How can we compute type properties (in our case, head shape) by
repeatedly unfolding datatype definitions without risking
non-termination on cyclic definitions? This question is the topic of
the present workshop submission.

\paragraph{Head tags and head shapes} \label{par:head-shapes} We
define the \emph{head tag} $h$ of a value to be either some
(non-unboxed) datatype constructor $C$ occurring at the head of the
value, or a datatype constructor among a fixed set of \emph{primitive}
constructors $\prim{t}$ with distinguished representations
($\prim{int}$, $\prim{string}$,
$\prim{array}$, $\prim{tuple}$,
$\prim{function}$, etc.), or the worst approximation $\anyH$
which contains all possible values. We want to compute a \emph{head
  shape} $H$ for any type expression $\tau$, which is just a list of
possible head approximations.
\begin{mathpar}
h \defequal C \defor \prim t \defor \anyH

H \defequal \emptyset \mid H, h
\end{mathpar}
If we know when two head tags have disjoint low-level representations,
and we know how to compute the head shape of a type expression, we can
easily check the disjointness condition for unboxed constructors
$C^\unboxed~\mathsf{of}~\tau$: the unboxing annotation is valid if the
tags in the head shape are pairwise disjoint.

In this document we do not discuss the first question (what is the
low-level representation of each head tag and which
definition of disjointness we use), which are low-level details
related to the OCaml implementation. We only discuss how to compute
those implementation-independent head shapes.

\section{Type unfolding with dynamic cycle detection}

\paragraph{Static or dynamic cycle detection?} The problem of cycles
also occur with type synonyms/abbreviations. OCaml will forbid cyclic
type abbreviations such as
%
\begin{lstlisting}
type 'a foo = ('a * 'a bar) and bar = 'a foo
\end{lstlisting}
%
This check is defined as a static check of well-formedness: the graph
of \emph{dependencies} from one abbreviation to another
(the definition of \lstinline{foo} mentions \lstinline{bar}) must be
acylic. Note that cyclic mentions in datatypes are allowed, for
example this is valid:
\begin{lstlisting}
type 'a foo = Cons of ('a * 'a bar)
and bar = 'a foo
\end{lstlisting}

We could follow the same approach, by considering that, unlike
constructors, unboxed constructors create dependencies in this sense:
a cycle of reference must go through at least one \emph{boxed}
constructor to be accepted. However, we found that this approach is
too restrictive in practice. For example:
\begin{lstlisting}
type 'a thunk = unit -> 'a
type 'a stream =
| Next of ('a * 'a stream) thunk [@unboxed]
| End
\end{lstlisting}
The static displine would consider that \lstinline{'a stream} depends
on itself (in an unboxed position) due to the occurrence of
\lstinline{'a stream} within an argument of \lstinline{thunk}. But if
we were to unfold the definition of \lstinline{thunk}, we would notice
that this recursive occurrence is under a function type, whose primitive tag
$\prim{function}$ does not depend on its input or output
types.

The dynamic detection mechanism we propose is more fine-grained than
the static check, and in particular accepts this declaration.

\paragraph{Naive dynamic cycle detection} A natural idea when
performing a series of unfoldings to compute a head shape is to
remember the set of type definitions that we have already expanded. If
we encounter a type definiton that is already in this visited set, we
are at risk of circularity and abort the computation, rejecting the
definition.

However, this approach is disappointing in practice. Consider for example:
\begin{lstlisting}
type 'a id = Id of 'a [@unboxed]
type t = Foo of int id id [@unboxed]
\end{lstlisting}
Computing the head shape of \lstinline{t} would unfold \lstinline{id}
once, then in turn compute the shape of \lstinline{int id}, and abort
as \lstinline{id} was already visited.

\paragraph{Call stacks}
The two occurrences of \lstinline{id} here are distinct and both occur
in \lstinline{t}. The second occurrence should thus count as
a (second) dependency of \lstinline{t} on \lstinline{id}, not a use of
\lstinline{id} within itself!

Our solution is to track, for each type subexpression of our input, the
definition in which it occurred (\lstinline{int id} occurs in
\lstinline{t}). More generally, we track the path of unfoldings that
led to this definition, which we call a \emph{call stack} for this
type expression. In this example, computing the shape of \lstinline{t}
(in the empty call stack) amounts to computing the shape of
\lstinline{int id id} in the call stack \lstinline{[t]}, remembering
that these expressions come from the definition \lstinline{t}, which
we can write \lstinline{((int[t] id)[t] id)[t]}. Unfolding this
definition brings us to the definition of \lstinline{id}, so we now
consider the expression \lstinline{(int[t] id)[t]} (the parameter of
the first \lstinline{id}) in the call stack \lstinline{[t, id]}. To
compute this shape, we unfold the remaining \lstinline{id}, but from
\lstinline{(int[t] id)[t]} we know that it comes from the call stack
\lstinline{[t]}; unfolding \lstinline{id} again in this stack is not
cyclic, we get \lstinline{int[t]}, and terminate with the primitive
tag $\prim{int}$.

Note: the name \emph{call stack} comes from viewing datatype
definitions as (mutually recursive) functions, and the problem of head
shape computation as the evaluation of a call to one of these
recursive functions. Our call stacks really correspond call stacks for
those functions, in the setting of \emph{call-by-name} evaluation
where the argument of a function is not computed until needed --- but
its call stack comes from its application site.

\section{Our algorithm}

Let us define a toy grammar for types $\tau$ and datatype declarations $d$.
\begin{mathpar}
\begin{array}{rcl}
\tau
& \defequal
& \alpha \defor (\tau_i)^i\:t
\\
d
& \defequal
& \typedecl {(\alpha_i)^i\:t} {\left(C_j\of\tau_j\right)^j} {\left(C_k^\unboxed\of\tau'_k\right)^k}
\\
\end{array}
\end{mathpar}
Each declaration comes with a family of (boxed) constructors and a family of unboxed constructors, either of which could be empty.

Our algorithm tracks the call stack in which type subexpressions
appear. We represent this with \emph{annotated types} $(\loc \tau l)$,
which contain a call stack $l$ at the top, and also on each subexpression.
\begin{mathpar}
\begin{array}{rcl}
\loc \tau l
& \defequal
& \loc \alpha l \defor \ploc {\Fam i {\loc {\tau_i} {l_i}} \: t} l
\\
l
& \defequal
& \varnothing\defor l,\,t
\\
\end{array}
\end{mathpar}

\paragraph{Head shape of a type declaration $d$}

\begin{mathpar}
\inferrule[Typedecl]
  { \jprod {\ploc {\Fam i {\loc {\alpha_i} \varnothing} \:t} \varnothing} R }
  { \jprod {\left(\typedecl {\Fam i {\alpha_i} \: t} \ldots {}\right)} R }
\end{mathpar}
To compute the head shape of a type declaration
%
$\typedecl {\Fam i {\alpha_i} \: t} \ldots {}$
%
we simply compute the head shape of the type expression
$\Fam i {\alpha_i} \: t$, with all type subexpressions annotated with
the empty call stack $\varnothing$. This type declaration will be
rejected by our implementation exactly if the computation returns
$\cycle$, or if two head tags with non-disjoint representations are
found in the result.

\paragraph{Head shape of an annotated type expression $\loc \tau l$}

We can now define our algorithm as a judgment
$\jprod {\loc \tau l} R$, which takes an annotated type expression
$\loc \tau l$ and returns a \emph{result} $R$, either a head shape or
a $\cycle$ error.
\begin{mathpar}
\begin{array}{rcl}
R
& \defequal
& H \defor \cycle
\end{array}

\inferrule[Var]
  { }
  {\jprod {\prod \alpha l} \anyH}

\inferrule[Cycle]
  { t \in l }
  { \jprod {\ploc {\Fam i {\loc {\alpha_i} {l_i}} \: t} l} \cycle }

\inferrule[Prim]
  { (t, \prim t) \in \prim T }
  { \jprod {\ploc {\Fam i {\loc {\alpha_i} {l_i}} \: t} l} {\prim t} }

\inferrule[Type]
  {
  t \notin l
  \\
  \typedecl {\Fam i {\alpha_i} \: t} {\Fam j {C_j\of\_\,}} {\Fam k {C_k^\unboxed\of\tau'_k}}
  \\
  \forall k,\quad\jprod {\substitute {\tau'_k} {\alpha_i \leftarrow \loc {\tau_i} {l_i}} {l, t}} {R_k}
  }
  { \jprod {\ploc {\Fam i {\loc {\tau_i} {l_i}} \: t} l} {\merge{\Fam j {C_j}, \Fam k {R_k}}} }
\end{mathpar}

When computing the shape of the datatype $\Fam i {\alpha_i} \: t$, the
datatype parameters $\alpha_i$ could get instantiated with types of any
shape. Our rule \Rule{Var} thus gives those type variables the shape
$\anyH$.

Before unfolding a type constructor, we check that it is not already
in our call stack. If it is, the rule \Rule{Cycle} aborts with
a $\cycle$ result.

We assume a relation $\prim T$ from certain datatype constructors to
primitive shapes, used in the rule \Rule{Prim}.

The rule \Rule{Type} performs the unfolding of a datatype
definition. Our input is a datatype
$\Fam i {\loc {\tau_i} {l_i}} \: t$ at some call stack $l$. We lookup
the definition of the datatype $\Fam i {\alpha_i} \: t$ in the global
datatype definition environment, split into a family of boxed
constructors and a family of unboxed constructors. The resulting shape
is obtained by concatenation of the tags of boxed constructors, and
shapes of unboxed constructor arguments: for each boxed constructor
$C_j \of \tau_j$ we add the tag $C_j$, and for each unboxed
constructor $C_k^\unboxed \of \tau'_k$ we add the shape of
$\tau'_k$. There are two subtleties in the rule:
\begin{itemize}
\item Computing the shape of a $\tau'_k$ may fail with a $\cycle$
  error; we use a $\merge \ldots$ operator that propagates this error,
  or concatenates the result shapes.

  We define it below.
\item The $\tau'_k$ mention the formal datatype parameters
  $\Fam i {\alpha_i}$ of the datatype declaration. We substitute them with
  the actual datatypes parameters $\Fam i {\tau_i}$ used the type
  expression at hand. More precisely, we have annotated types
  $\Fam i {\loc {\tau_i} {l_i}}$, which we substitute under
  a non-annotated type $\tau'_k$. To get an annotated type as
  expected, we need to use a call stack for all type subexpressions of
  $\tau'_k$ that are not variables (each variable $\alpha_i$ gets the
  call stack $l_i$ from our input). Those subexpressions get the call
  stack $l,t$, tracing the fact that they come from the expansion of
  $t$ in the current context $l$.

  To summarize, our substitution operation $\substitute \tau \sigma l$
  takes an unannotated type $\tau$, a substitution $\sigma$ from its
  type parameters to \emph{annotated} types $\loc {\tau_i} l$, and an
  ``ambiant call stack'' $l$ to use on all non-variable type
  expressions. We define it below.
\end{itemize}
\begin{mathpar}
\begin{array}{lcl}
\merge {\ldots, \cycle, \ldots}
& =
& \cycle
\\
\merge{(H_i)^i}
& =
& (H_i)^i
\\
\end{array}

\begin{array}{rcl}
\substitute \alpha \sigma l
& =
& \sigma(\alpha)
\\
\substitute {\Fam i {\tau_i} \: t} \sigma l
& =
& \ploc {(\substitute {\tau_i} \sigma l) \: t } l
\end{array}
\end{mathpar}

\subsection{Termination}

We have a proof sketch (not a complete proof yet) of termination that
goes as follow.

We say that the \emph{parent} of a non-empty call stack $l,t$ is the call stack $l$.

We have to prove that any potentially-infinite derivation of
$\jprod d R$ is finite. It suffices to prove that any path of
applications of the rule \Rule{type} within such a derivation is
finite. Indeed, the three other rules, \Rule{Var}, \Rule{Cycle} and
\Rule{Prim}, terminate the derivation, and the width of each
\Rule{Type} application is bounded by the maximal number of
constructors of a type declaration in the global environment (which we
assumed finite.

For a path $P$ of \Rule{Type} applications we look at the input types
$\Fam {p \in P} {\loc {\tau_p} {l_p}}$ along this path, and in
particular the path of locations $\Fam {p \in P} {l_p}$. (We order $P$
by position in the derivation, with the smallest element $p_0$
corresponding to the root of the derivation.)

\begin{lemma}
  If the stack $l_p$ occurs at position $p$ in the stack, all prefixes
  of $l$ can be found at some earlier position $p' \leqslant p$.
\end{lemma}

We build a \emph{tree} structure on this path $P$. Note: the tree is
not the derivation, is is overlayed over one linear path.

The root of the tree is the first element $p_0$ of the path. Remark
that it is the unique element with the empty call stack
$\varnothing$.

Any other element of the path has a call stack $l, t$; we place it as
a children of the closest element earlier in the trace with the parent
call stack $l$, which must exist by the lemma above. By construction,
the parent of a child in the tree has the parent call stack.

Our termination argument is that this tree is finite (so the trace
is finite):
\begin{itemize}
\item Its height is bounded: a call stack $l$ can only mention each
  datatype once, so the height of any call stack (and thus of
  the tree) is bounded by the number of datatypes in the global
  environment.
\item Each node has a finite number of children. Intuitively the
  argument is if a node corresponds to some type
  ${\Fam i {\tau_i}} \: t$, each child corresponds to one
  sub-expression occurring in the expansion of the datatype $t$, with
  each sub-expression occurring at most once among the
  children. (This is the delicate part of the proof.)
\end{itemize}

\bibliography{biblio}
\end{document}
