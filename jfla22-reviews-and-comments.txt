> ----------------------- REVIEW 1 ---------------------
> SUBMISSION: 14
> TITLE: Déboîter les constructeurs
> AUTHORS: Nicolas Chataing and Gabriel Scherer
> 
> ----------- Overall evaluation -----------
> SCORE: 0 (borderline paper)
> ----- TEXT:
> # Déboîter les constructeurs
> 
> ## Résumé
> 
> L'article présente une nouvelle fonctionnalité du langage OCaml : la possiblité
> d'annoter les constructeurs d'un type algébrique avec des annotations de
> "déboîtement" (unboxing) pour éviter une indirection dans la représentation
> mémoire du dit constructeur. Toutefois, certains déboîtement sont incompatibles
> avec l'injectivité des constructeurs, et les annotations correspondantes doivent
> donc être rejetées par le compilateur OCaml.
> 
> L'article motive et décrit le problème du déboîtement de constructeurs, avant de
> présenter une analyse statique qui rejette les annotations invalides. Cette
> analyse statique fonctionne par un calcul de point fixe. L'article propose une
> preuve de la terminaison de cette analyse, qui est non triviale en la présence
> de définitions cycliques de types, comme c'est le cas en OCaml.
> 
> ## Évaluation
> 
> L'article présente un problème bien motivé et compréhensible par tous les
> programmeurs fonctionnels. Résoudre ce problème serait d'un grand intérêt
> pratique et mettrait potentiellement en jeu des questions théoriques
> intéressantes. Il me semble donc que c'est un article idéal pour le public des
> JFLA, notamment pour sa communauté OCaml importante.
> 
> J'ai toutefois deux réserves à son sujet.
> 
> Tout d'abord, du point de vue du fond, je pense que le cadre utilisé n'est pas
> le bon. L'article remarque et décrit très bien comment les types d'un langage
> suffisamment riches peuvent être vus comme des λ-termes simples. Cette
> observation a déjà été exploitée à de nombreuses reprises, notamment dans le
> cadre du compilateur Haskell GHC où les sortes (types des types) contrôlent
> finement la représentation des données. Malheureusement, le présent article se
> contente d'énoncer cette observation avant de revenir manipuler les types à un
> niveau à mon sens beaucoup trop concret et bas-niveau. Le tout mène à des
> notations absconses et à un cadre beaucoup trop ad-hoc. La correction de
> l'analyse n'est jamais démontrée, ni même énoncée précisément.

Quelle est la propriété qu'on cherche à établir ?
- étant donnée un choix de représentation R : valeur -> data
    telle que
      R(C^unboxed v) = R(v)
      R(C v) = R(C v')  pour tous v, v'
      R(v \in \prim(t)) \in R(\prim(t))  (pour R: primtypes -> P(data))
      et R | \prim(t) injectif
  nous n'acceptons [@unboxed] que s'il n'introduit pas de confusion
    (R reste injectif)

Comment on l'établit ? On décompose ça en
- une normalisation faible des types qui déplie les définitions
    tau => whnf(tau)
- une approximation de représentation sur ces formes normales,
  qui vérifie au passage la séparation
    Rel(whnf(tau), P(data))
    (si tau ~ S, alors (R | tau) injective)

DONE la partie "sémantique des têtes" a été ajoutée pour ŕepondre en
partie à cette demande d'un énoncé précis.

Note: dans le cas où on n'a pas séparation, on pourrait imaginer
renvoyer comme témoin une paire de valeurs distinctes mais de même
représentation. Mais dans le cas général cela demande de produire des
valeurs au type des paramètres des constructeurs, ce qui n'est pas
forcément facile en présence de GADTs, et pas dans l'esprit de la
forme normale faible qui ne va pas sous les constructeurs.

> Je peux toutefois comprendre que le but des JFLA est aussi de proposer à la
> communauté des travaux de recherches qui ne sont pas totalement aboutis. C'est
> pourquoi le deuxième problème est à mon sens plus grave.
> 
> L'article, à partir de sa troisième section, est très mal écrit.  La lecture du
> texte comme du formalisme est souvent difficile. Le manque de soin apporté à la
> rédaction laisse l'impression désagréable d'un travail bâclé. Ce manque de soin
> s'observe à tous les niveaux, des plus superficiels aux plus fondamentaux :
> hauts de page manquants ; usage intempestif des couleurs; style familier et abus
> de guillemets ; notations prenant à rebours les conventions mathématiques
> (exposants pour indices) ; liens logiques ténus entre les phrases et paragraphes
> ; termes utilisé si approximativement qu'ils en deviennent faux (corollaire
> p. 13 trivial dans son énoncé actuel puisque vrai de tout jugement inductif) ;
> preuve informelle mais justifiée par ce qui évoque (involontairement ?) un
> argument d'autorité ; bibliographie minimale ; pans entiers et intéressants du
> travail en annexe de l'article (alors qu'il reste plus d'une page libre !).

DONE la section est récriture

> Pour cette raison, je recommande que l'article soit accepté uniquement si la
> troisième section est intégralement réécrite, et les sections précédentes
> harmonisées en conséquence. Il me semble que le théorème de terminaison, dont la
> preuve occupe actuellement plus d'une page, devrait être renommé en conjecture
> et déplacé en annexe pour laisser place au contenu des annexes actuelles, bien
> plus intéressant (notamment la gestion du filtrage de motifs).
> 
> ## Points de détail
> 
> Remarques générales :
> 
> - J'aurais traduit "to unbox" en "déballer" plutôt que le littéral "déboîter",
>   plus conforme avec l'acception courante du terme anglais.

Je trouve que penser à une indirection dans le tas comme une "boîte"
est plus parlant que comme un "emballage". Quand j'explique le type
('a ref) à mes élèves je leur parle d'une boîte, un tiroir, qui
contient un élément.

Ceci dit, déballer n'est pas honteux non plus (déboîter est rarement
utilisé en français, et déballer traduit aussi bien l'idée d'enlever
des couches superficielles inutiles de la représentaion). Quand on
hésite il vaut mieux aller dans le sens du rapport pour lui faire
plaisir, alors allons-y. (Mais: on est juste en temps donc ce ne sera
peut-être pas prêt pour la première version camera-ready.)

TODO renommer {dé,em}boît* en {dé,em}ball*

Je reste hésitant. Mettons ça en bas de la file de priorité pour l'instant.

(Remarque: J'ai vu depuis cette hésitation interne un exposé de Pierre
Chambart qui utilise aussi déboîter.)

> - Le orange est abominable et fait ressembler certaines phrases qui combinent
>   orange, violet, rouge et \texttt{} en guirlandes de Noël criardes.

On peut changer le ton du orangé (ça doit être une couleur LaTeX
par défaut), mais je vote pour garder les guirlandes de Noël. On
pourra discuter avec le rapporteur si on a le temps pour affiner la
palette de couleur, mais ce n'est pas la priorité.

DONE j'utilise maintenant le Magenta de [dvipsnames]{xcolor}
( https://en.wikibooks.org/wiki/LaTeX/Colors ), qui rend bien aussi,
mais on perd peut-être le côté Noël...

> p. 1
> 
> - Une des phrases du résumé n'est pas terminée.

DONE

> - "chiffre binaire (bit)" superflu.

Ok pour juste "bit" (sans couleur) pour faire plaisir.

DONE

> p. 2
> 
> - "pouillème" : familier.

Et alors ?
Aux JFLA on est entre ami-e-s, non ?

NO CHANGE

> - "fait sens" : anglicisme.

DONE

> - "disjointude" : séparation.

DONE pour faire plaisir, mais je n'aime pas trop ce choix : on dit
bien de deux ensembles qu'ils sont "disjoints", te non "séparés", donc
il faudrait un substantif formé sur la même racine. "disjointude" est
un néologisme un peu ridicule, mais le reste de l'article est un peu
ridicule par moments donc c'était cohérent. Eh bien (Oh well)...

> - Parmi la liste de contributions, seules les trois premières ont été réellement
>   traitées dans le corps de l'article !

DONE mentionné que les points de la fin sont en annexe.
(Il n'y a rien de honteux à mettre des choses en annexe si on n'a pas
la place de tout faire tenir confortablement dans une limite de page.)

> p. 3
> 
> - "short path" se dirait plutôt "fast path" et ne se traduit pas vraiment par
>   "raccourci" (qui ne permet pas de traduire aisément "slow path").

À y réfléchir, je préfère rester sur raccourci, qui me semble bien
traduire l'idée. Un short/fast path (ok pour dire "fast path"), c'est
un cas où on peut faire plus rapide que d'habitude parce qu'on peut
éviter un calcul plus général qui n'est pas utile dans ce
cas-là. C'est un peu comme couper par un chemin plus rapide mais qui
n'est pas utilisables pour toutes les destinations ou pour tout le
monde (plus raide, etc.). Pour "slow path" on peut toujours dire
"rallongi".

NO CHANGE

> p. 6
> 
> - La notation mathématique usuelle pour les familles consiste à avoir l'indice
>   de la famille en... indice.

J'aime mieux le mettre en exposant puisqu'on écrit A^B pour le type
des familles de A indicées par des éléments de B, et il y a une
confusion visuelle possible entre foo_i le i-ème élément et (foo)_i la
famille, mais ok pour mettre en indice si c'est standard.

DONE

> - "Ce jugement a des problèmes de terminaison" : impropre puisqu'un jugement
>   inductif classifie des dérivations finies par définition.

... ou alors on se demande s'il existe ou non des dérivations infinies
(on prend le plus grand point fixe plutôt que le plus petit point fixe
des règles d'inférence), ce qui est évidemment la question ici.

> - "représente un (multi)ensemble" les parenthèses ne peuvent que dérouter le
>   lecteur, puisqu'il s'agit bien d'un multiensemble.

DONE

> - Le paragraphe qui suit les grammaires est difficile à lire, parce que pas
>   assez direct à mon avis. De plus, la notation est incohérente avec la
>   grammaire, puisqu'on utilise "," plutôt que "+".

DONE , => +

> - Le système, sans hypothèse supplémentaire sur T^, est non-déterministe. Est-ce
>   normal ? Pourquoi ne pas avoir séparé les types de base dans la grammaire ?

Ok pour séparer dans la grammaire. Attention, dans la grammaire, les
types primitifs sont des constructeurs de types qui peuvent avoir des
paramètres, par exemple
  (int -> bool)
est du sucre syntaxique pour
  (int, bool) \hat{function}

DONE finalement nous avons juste ajouté une remarque qui demande à ce
qu'il n'y ait pas de recouvrement entre types primitifs et
déclarations de types.


> 
> p. 7
> 
> - La définition précise de la séparation n'a jamais été donnée.

Le truc qui est casse-pieds est que la notion de séparation est
relative à un choix de représntation des données; on peut déjà
détecter des conflits dans la forme de tête "abstraite", mais il reste
d'autres conflits si deux types primitifs, par exemple, ont une
représentation bas-niveau non-disjointe. En même temps les détails de
la représentation bas-niveau ne sont pas le sujet de l'article, et en
parler trop limiterait la portabilité du travail à d'autres langages
utilisant d'autres représentations.

DONE section "Sémantique des têtes".

> - La "monotonicité" (monotonie) du jugement n'a jamais été démontrée, ni même
>   énoncée formellement.

Et alors ? C'est simple à énoncer

  si      shape(tau) <= shape(tau')
  alors   shape(tau''[tau/alpha]) <= shape(tau''[tau'/alpha])

et à prouver (une induction standard), et c'est juste une remarque en
passant et pas une propriété importante pour l'article.

DONE j'ai retiré cette remarque, c'était le plus simple pour prendre
en compte ce retour.

> - Que se passe-t-il en présence de types abstraits ou opaques ?

L'implémentation utilise ⊤ comme approximation des types abstraits --
évidemment, et c'est ce qui est cohérent avec le traitement des variables
de type.

TODO mentionner cela dans le texte

TODO envisager de parler des types abstraits explicitement dans la formalisation ? on aurait alors trois catégories:
- types primitifs (dans \hat{T})
- types abstraits (pas de définition)
- types de donnéees (définition disponible)

Remarque: si on fait cette distinction, autant garder une seule notion
de constructeur de type dans la grammaire, au lieu de séparer au
niveau de la grammaire comme suggéré plus haut.

DONE finalement j'ai décidé de garder un cadre minimal dans le corps
du document, où on garde juste les types primitifs et les types de
données. J'ai complété la discussion des types abstraits en annexe.

> Modularité ? Sorte pour les summaries ?

Notre approche est effectivement non-modulaire, elle repose sur
l'expansion des définitions et non sur une définition de
signature/summaries pour abstraire les types utilisés. Il y a une
discussion au long cours à ce sujet entre les mainteneurs OCaml
(moi-même d'un côté, qui défend les signatures modulaires; Stephen
Dolan et Leo White, de l'autre, qui recommandent une approche
non-modulaire pour les propriétés des types;
cf. https://github.com/ocaml/ocaml/pull/10017). Nous avons fait ici le
choix de la non-modularité pour suivre les choix de conception de
Dolan et White sur des propriétés similaires ("immediateness" et
sortes pour "value types"), en prévision de collaborations et
d'interactions à venir entre leur travail et le nôtre.

Détails sur le débat d'écoles. En ce moment (et depuis des années) le
typage des applications de foncteur ne traite pas bien les propriétés
calculées de façon modulaire (comme la variance): on oublie de
re-calculer la signature des types après instantiation des foncteurs.
La variance d'un type dans un corps de foncteur peut être raffinée
s'il déṕend d'un type venant du paramètre du foncteur, et que celui-ci
a été instancié par une définition concrète, mais OCaml ne fait pas ce
recalcul. Il faudrait l'implémenter (et tout le monde a la flemme), et
il y a des inquiétudes quand au coût en performance sur les bases de
code utilisant beaucoup de foncteurs (ce qui donne encore
moins envie). L'approche non-modulaire, en plus d'être plus simple
à implémenter, est donc plus précise en pratique. Les hérauts de la
non-modularité ont encore quelques arguments:

- l'approche non-modulaire calcule les propriétés à la demande, au
  lieu de pré-calculer les summaries pour chaque déclaration, ce qui
  est typiquement un peu plus efficace (elles sont rarement demandées)

- l'approche modulaire demande à modifier le format des signatures
  (les fichier .cmi) à chaque nouvelle propriété qu'on introduit, ce
  qui introduit des difficultés variées d'outillage
  (cf. https://github.com/ocaml/ocaml/pull/9592 par exemple).

TODO devrions-nous envisager de discuter de ces aspects en annexe ?
(On pourrait demander à Adrien son avis.)


DONE Dans l'article: nous avons étoffé un peu le traitement des
frontières d'abstraction, qui était mentionńé au passage dans l'annexe
B.2 (abstract types), en le transformant en un nouvel annexe B.3
(Signatures, paramètres, modularité).


> Bidouille ? Informelle ?

Nous avons repris ce paragraphe pour détailler ce que nous entendons
(précisément) par "normaliser".

> p. 8
> 
> - La remarque sur l'instrumentation est superflue.

Pas convaincu. Quand nous avons expliqué au ML workshop que notre
technique de détection de la non-terminaison est dynamique, les
rapports ont tiqué sur ce point en disant : "ce n'est pas dynamique si
c'est fait pendant le typage". Il s'agit ici d'expliquer qu'on
surveille (dynamiquement) l'expansion des définitions de type. Ce
n'était pas évident pour une partie de notre lectorat, donc ça vaut la
peine d'insister.

NO CHANGE

> p. 9
> 
> - Je ne suis pas certain de comprendre la grammaire, et plus précisément le rôle
>   joué par l à droite de "::=". S'agit-il de la définition d'une famille
>   inductive dont l est un indice ?

DONE j'ai clarifié la présentation de la grammaire

> 
> p. 10
> 
> - Abus de guillemets.

DONE ?

> p. 11
> 
> - La section 3.4 est très intéressante mais n'est pas vraiment exploitée par
>   l'article, comme remarqué plus haut. Le fonctionnement des constructeurs
>   "emboîtés" (ou plutôt "emboîtants") évoque celui des _thunks_.

Oui, les constructeurs boîtants sont réduits comme des constructeurs
paresseux, alors que les constructeurs déboîtés sont réduits strictement.
 
> - Il me semble que les deux phrases "on oublie" ont interverti "déboîtés" et
>   "emboîtés".

GONE (cette partie est récrite)

> - Le dernier paragraphe de 3.4 est bien trop rapide.

GONE (cette partie est récrite)

> p. 13
> 
> - "unboxé" : incohérent avec le parti pris de traduction systématique du reste
>   de l'article.

DONE

> - Le théorème est mal écrit (les règles des mathématiques n'affranchissent pas
>   de celles du français) et trop informel : les termes employés n'ont pas tous
>   été définis précisément, ce qui me laisse dans l'incapacité de le vérifier.
> 
> - Les remarques concernant la réalisation de la preuve sont malvenues à ce stade
>   (contrairement aux remerciements qui concluent l'article).

Bof. Je ne trouve pas honteux de mettre des détails "boutique" sur la
preuve dans la partie sur la preuve.

DONE en faire un paragraphe "Acknowledgments".

> p. 15
> 
> - "redexes" : radicaux.

DONE

> - "Nous manquons de place" : l'article dispose encore d'une page libre,
>   cf. l'appel des JFLA qui précise bien que les soumissions disposent de seize
>   pages _hors bibliographie_.

Adrien va être heureux, avec le formalisme en plus dans la preuve nous
arrivons tout juste à 16 pages.

> ----------------------- REVIEW 2 ---------------------
> SUBMISSION: 14
> TITLE: Déboîter les constructeurs
> AUTHORS: Nicolas Chataing and Gabriel Scherer
> 
> ----------- Overall evaluation -----------
> SCORE: 2 (accept)
> ----- TEXT:
> Cet article traite une optimisation de la représentation des types somme
> fournis par des langages comme OCaml, Haskell, SML, Rust, etcétéra.
> Normalement une valeur d'un type somme est représentée par un entier quand
> cette valeur n'a pas d'arguments, ou sinon, par une référence vers un bloc
> étiqueté. Une optimisation standard, le « déboitement », permet de
> simplifier la représentation pour un type qui n'a qu'un seul constructeur
> avec un seul argument. Dans cet article est étudiée une généralisation du
> déboitement qui s'applique quand les différents cas du type peuvent être
> distingués par leurs représentations seules et donc sans l'intermédiaire
> d'un bloc étiqueté. Un exemple simple, mais convaincant est présenté pour
> expliquer et motiver le travail. Une formalisation précise définit quand
> l'optimisation peut être appliquée, le problème principal est de détecter
> et rejeter les définitions qui deviennent cycliques quand l'optimisation
> est appliquée. Une preuve de terminaison de l'algorithme de détection est
> présentée. Les annexes présentent une implémentation de l'approche dans le
> compilateur OCaml et de possibles extensions.
> 
> Je trouve cet article très intéressant et, à part les sections 3.4 et 3.5,
> agréable à lire. L'introduction présente le problème clairement, l'étude de
> cas montre bien l'utilité de l'optimisation, la formalisation des sections
> 3.1, 3.2 et 3.3 est précise et révèle bien les surprenantes subtilités du
> problème. Par contre, j'ai moins bien compris la discussion de la forme
> normale (la section 3.4) et la preuve de terminaison (la section 3.5).
> J'ai dû mal à juger si cette preuve est inutilement compliquée ou si c'est
> juste que sa présentation n'a pas été assez travailler. Quelque part, je
> trouve le contenu des deux annexes plus instructif. Je me demande même si
> la modélisation en OCaml décrite dans l'annexe A n'est pas plus facile à
> comprendre que les prédicats des sections 3.1 à 3.3. Serait-il raisonnable
> d'exprimer l'algorithme comme un petit programme OCaml (sans tous les
> détails techniques de l'implémentation dans le vrai compilateur) et de
> raisonner sur ce programme directement pour démontrer la terminaison ?
> 
> Serait-il possible de dire quelques mots sur le traitement de déboitement
> fait ou non par les compilateurs Haskell (ghc), Rust, SML (MLton et
> Poly/ML), par exemple ?

TODO ? Dire les choses précisément représente beaucoup de boulot, et
je ne suis pas sûr d'avoir le temps pour la deadline camera-ready...

À ma connaissance:

- Rust implémente l'unboxing automatiquement, généralisé avec des
  règles de "niche", c'est plus facile dans un cadre où on
  monomorphise tout. (Ils peuvent déboîter Option<A> par exemple une
  fois que A est connu, dès que A n'a pas de valeur 0 valide.)

- Haskell propose un "newtype" qui déboîte les définitions à un seul
  constructeur (comme [@@unboxed] actuel dans OCaml) mais pas plus. Il
  y a du support pour des types unboxés (et non passables à des
  fonctions polymorphs) qui doivent permettre de faire des choses
  approchantes à la main dans certains cas, mais plus péniblement.

- MLton élimine parfois des couches de boxing par une optimisation de
  "flattening", par exemple `(int * int) array` va implicitement
  unboxer `int * int` (en une paire de mots plutôt qu'un bloc
  à deux arguments) et utiliser un tableau dont les éléments font deux
  mots. Ce n'est pas comparable: on gagne dans des cas différents.

Par ailleurs, GHC et MLton essaient d'éliminer des applications de
constructeurs dans un programme spécifique: si on appelle `f (C v)` et
que `f` commence par déconstruire `C`, on va essayer de transformer le
programme pour éliminer la construction et la déconstruction. (On peut
voir ça comme un cas simple de déforestation.) L'inlining peut le
faire, mais augmente beaucoup la taille du code, et des optimisations
de worker/wrapper plus spécialisées sont préférées.
(OCaml le fait un peu, mais relativement peu dans le cas général de
constructeurs de type somme, il se concentre sur les cas spécifique
des types primitifs boxés (float, int{32,64}) et des fonctions
non-currifiées.)



> 
> Remarques mineures
> ------------------
> p.1
> * Au risque de compliquer l'introduction, il serait plus clair, je pense, de
>   préciser que par bloc mémoire, vous voulez dire un pointeur vers un bloc
>   mémoire.

DONE
 
> p.2
> * "Juin 2016" -> "juin 2016"
> * "est très rapide, et les programmes" -> "est très rapide et les programmes"

DONE

> p.3
> * "du code a'performances égales" -> "du code à performances égales"
> * "de bonnes performances" -> "de bonnes performances."

DONE
 
> p.5
> * le terme "raccourci" a déjà été introduit (donc, pas besoin de le colorer).
> * "“sans dépassements”" -> * "« sans dépassements »" (et ce texte dépasse la
>   marge...)

DONE

> p.6
> * premier paragraphe de la section 3.1 : les parenthèses pourraient être
>   remplacées par de simples virgules et le texte "par soit une liste" n'est
>   pas très clair.

DONE

> * "*Nous notons... pas d'importance.)" : les parenthèses ne semblent pas
>   être nécessaires ici.

DONE

> p.7
> * "dans ce jugement, et on" -> "dans ce jugement et on"
> * "À l'inverse, quand calcule ... comme intid ..."
>   -> "À l'inverse, quand on calcule ... comme int id ..."
> * "une intersection non-vide" -> "une intersection non vide"

DONE

(Et si les gens nous laissaient placer les virgules comme on veut ? Ce
serait trop beau...)

> p.8
> * "un type de flots (stream)" -> "un type de flots"
> * "déejà" -> "déjà"

DONE

> p.9
> * "une structure de donnée complexe" -> "une structure de données
>   complexe" ?
> * "(Par contre, ... jugement.)" : les parenthèses ne semblent pas être
>   nécessaires ici.
> * "la règle Typedecl0 l'expression" -> "la règle Typedecl0 par l'expression"

DONE

> p.10
> * "annottées" -> "annotées"
> * "la regle Type" -> "la règle Type"
> * *** "car la premisse t \in l est invalide"
>   -> "car la premisse t \not\in l est invalide" ***

DONE

> p.11
> * "Caculer" -> "Calculer"
> * "novueau" -> "nouveau"
> * "mutuellemnt" (https://www.gnu.org/software/ispell/ ou, mieux, mais payant,
>   https://www.antidote.info/fr ...)

DONE
(Le rapport a raison d'être un peu narquois sur les typos, il y en a beaucoup...)
 
> p.12
> * "qui récrit" -> "qui réécrit"

Bof, je crois que les deux sont corrects.

> * "par autre sous-arbre"
>   -> "par un autre sous-arbre"

DONE

> p.13
> * "noeuveau" -> "nouveau"
> * "unboxés" -> "déboités"

DONE

> p.17
> * "nativint" -> "nativeint"
> * "nous suivant" -> "nous suivons"

DONE

> p.18
> * *** "pareillement les constructeurs constants"
>   -> "pareillement les constructeurs non constants" ***
> * "En absence de tableaux" -> "En l'absence de tableaux" ?

DONE

> p.19
> * En bas de page : je ne comprends pas pourquoi il y a quatre cas dans le
>   switch, mais que 2 dans la forme de tête.

DONE clarifié l'exemple

> ----------------------- REVIEW 3 ---------------------
> SUBMISSION: 14
> TITLE: Déboîter les constructeurs
> AUTHORS: Nicolas Chataing and Gabriel Scherer
> 
> ----------- Overall evaluation -----------
> SCORE: 3 (strong accept)
> ----- TEXT:
> L'article présente une extension de concept de constructeur déboité déjà présent dans le compilateur OCaml, permettant de supporter de deboiter un (ou plusieurs) constructeur(s) dans une définition de type arbitraire là ou le système actuel ne supporte que le cas ou le type ne déclare qu'un seul et unique constructeur.
> 
> Le déboitage de constructeur ('unboxing' en anglais) est une optimisation de la représentation bas niveau des types en OCaml, et dans certains cas, peut être particulièrement utile pour des raisons de performance, comme par exemple le cas de la bibliothèque Zarith qui est analysé dans l'article, et qui a besoin de l'extension proposé par l'article (sans cette extension, la version courante de zarith est forcé d'utiliser les fonctions dangereuses du module Obj).
> 
> L'extension présenté dans l'article principalement en un algorithme de vérification de la correction d'annotation de déboitage sur les constructeurs, avec une preuve que l'algorithme est complet et terminant. De plus, l'algorithme présenté est conçu pour autoriser de nombreux cas non-triviaux impliquant des définitions de type récursives.
> 
> L'article présente avec clarté les résultats principaux de l'article (algorithme de vérification des annotations, et preuve de terminaison et de complétude), mais aussi les tenants et aboutissants du déboitage des constructeurs, qui est une optimisation qui est désiré de de longue date par une partie de la communauté OCaml (et d'ailleurs assez régulièrement émulé par le biais de fonctions du module Obj qui court-circuitent le typage).
> 
> 
> 
> ----------------------- REVIEW 4 ---------------------
> SUBMISSION: 14
> TITLE: Déboîter les constructeurs
> AUTHORS: Nicolas Chataing and Gabriel Scherer
> 
> ----------- Overall evaluation -----------
> SCORE: 0 (borderline paper)
> ----- TEXT:
> Ceci est une méta-review.
> 
> L'article est accepté sous condition de modifications assez
> conséquentes, et d'une relecture finale. Les modifications principales
> demandées sont les suivantes :
> - une réécriture du paragraphe 3 (voir les reviews 1 et 2)
> - l'intégration d'une partie des annexes à l'article, par exemple à la
>   place de la preuve de terminaison
> et naturellement prendre en compte les autres remarques des relecteur·rices.
> 
> Merci d'indiquer rapidement à Chantal Keller <Chantal.Keller@lri.fr> si
> vous acceptez ces modifications.
> 
> En cas d'acceptation, un membre du PC pourra aider dans une phase de
> shepherding. La version finale sera à soumettre pour le 7 décembre. Nous
> rappelons qu'une relecture de cette version finale sera effectuée pour
> valider l'acceptation définitive.

