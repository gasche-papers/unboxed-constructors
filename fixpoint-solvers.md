## A faster solver for general systems of equations
Christian Fetch, Helmut Seidl, 1999

No monotonicity hypothesis for the update functions. (They compute
a minima of the new result and previous result?)

Partial solutions: one chooses a subset of variables of interest,
and we try to compute a least fixpoint *of those variables*.

They describe two existing solvers

- W, the "worklist solver', attributed to
  + B. Vergauwen, J. Wauman, J. Lewi, 1994
    Efficient xpoint computation, i
  + N. Jørgensen, 1994
    Finding fixpoints in finite function spaces using neededness analysis and chaotic iteration

  When the evaluation of an update function for X needs a variable Y,
  we store the dependency (X should be updated if Y increases), add
  Y to the worklist of nodes to evaluate next, and provide the
  "current known value" for Y.

- TD, the "top down solver", attributed to
  + Le Charlier and Van Hentenryck, 1992
    A universal top-down fixpoint algorithm

  The basic operation on a variable X is not to call its update
  function once, but to "solve" it by iteratively calling the update
  function unless it becomes "stable", it has reached a fixpoint for
  the current valuation.
  
  We maintain a set "Stable" of variables that have reached a fixpoint
  in this sense.

  When the evaluation of an update function for X needs a variable Y,
  - if Y is stable, read its valuation directly
  - if Y is not stable,
    we solve it first before returning its stable value,
    unless we are already in the process of evaluating Y,
    in which case we read its valuation directly
    
  Whenever a variable increases, we "destabilize" it by removing it
  from the Stable set and recursively destabilizing all variables that
  depend on it.

  This algorithm is called "top down" because it computes variables in
  a depth-first manner. In particular, if there are no cyclic
  dependencies, a single "solve" call computes the final value of
  a variable after computing the final values of all the variables it
  depends on.

The authors observe that W has better complexity guarantess but that
TD works better in practice. They propose a more complex variant of W,
using timestamps on variables and a priority queue, that gets some of
the benefits of TD and perform better than W and TD in their program
analysis examples.

They remark that:

- WRT works better than TD on strongly connected components

- TD automatically ignores "dead" variables, that are not needed by
  anyone anymore, while worklist-based algorithms may still go on to
  update them. (If a variable was needed in the past and one of its
  dependencies is updated, it may be added to the worklist despite not
  being interesting anymore.)


## Lazy Least Fixed Points in ML

François Pottier, 2009 (unpublished)

A cute implemnetation in OCaml. Implements either algorithm W or TD --
more precisely, a variant of TD with a worklist.


