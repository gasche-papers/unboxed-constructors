We would like to thank the reviewers for their excellent feedback. We found deeply engaging remarks and questions in each of your reviews (this experience may be a first for us). Thanks!

Our response has the following format:

- a summary of the most important points
- proposal for a revision plan (as proposed by the PC chair)
- a detailed discussion of the many interesting points you raised in your review, per review; please obviously feel free to skip this (even the parts on your own review).


### Salient points

#### (Lack of) benchmarks

The most salient criticism is the entirely fair comment in review B that our experimental evaluation is limited. (This is intentionally not a PLDI-style paper where an experimental evaluation of performance metrics is the key evaluation criterion of the proposed contribute.)

In the "detailed discussion" section of our response, we:

- provide more details on the Zarith benchmark

- mention another performance experiment performed by Jeremy Yallop where similar-ballpark performance gains of up to 30% were observed

- discuss a potential use-case in the `native_compute` machinery of Coq

- mention another use-case to define a quasi-universal type of OCaml values as a sum type, to make runtime introspection code more ergonomic

Most importantly, we argue that the value of this sort of language features is not in improving existing performance-sensitive programs, and that other forms of evaluation are needed. (In short, the methodological problem with existing programs is that those programs, when they are known to be performance-sensitive, were written with the current feature set in mind. So they will use other approaches than sum types if they cannot afford the overhead of constructor boxing. This is a usual problem with most performance-oriented language features.)

#### Place of the proof

Review A would want to see more of the soundness proof of the on-the-fly termination checking algorithm in the paper. The extra 3.5 pages offered by the chair would not suffice to include the appendices in full (even if we focused on sections A.1 and A.2, up to the definition of the termination measure), but we propose to include a high-level sketch of the argument.

(This would be at the detriment of including more content of Dave Prosser's algorithm in the paper itself, for example Section B.1 which may be of interest to a different audience. These space-restriction questions are always difficult and we welcome any further feedback of the reviewers on this question.)

#### Mistake on GADTs

Review A remarks that our "optimized" treatment of GADTs in Section 5.3 is wrong. This remark is correct, we apologize for the mistake -- this is a language design aspect that we had given some thought about but not implemented or reasoned formally about. We will remove this section from the paper.

#### New related work

In July we became aware of the paper "Bit-stealing made legal" presented last week at ICFP on a similar topic ( https://inria.hal.science/hal-04165615/ ). In short, we believe the two works are complementary: they go further than we do in changing representation (by choosing new representations for types rather than detecting compatibility between current ones), while they leave as future work the problem of avoiding infinite recursion during unrolling, a central focus of our work. We are now in touch with the authors and will include a detailed comparison in the related work, informed by our discussions.

In more details:

- They propose a DSL of value representations, similar in spirit to the older work on Hobbit [Diatchki, Jones, Leslie, 2005] -- and richer than Dargent as optimizations involving sum types are considered in details. This is more expressive than our focus on constructor unboxing, it expresses more transformation. A minor downside is that it is more low-level (harder to use and to make portable across different implementations).

- They include a formal (pen-and-paper) specification of disjointness. We will have to weaken our claim in lines 1064-1066 that disjointness of sum types representations has not been formally discussed in existing academic work. (We consider that the two works are independent/simultaneous, so in a sense we could both claim novelty.)

- We have a working experimental integration of our feature in a production compiler -- the integration in a complex codebase was non-trivial and we consider this a part of our contributions. Their implementation is a small prototype that just compiles pattern-matching definitions in a model language, it was not possible for them to measure the performance impact on real programs in an existing programming language.

- We support recursive definitions and provide a formal treatment of the necessary normalization procedure. Our discussion of Dave Prosser's cpp algorithm is also a complementary contribution.


### Proposed plan for a revision

We propose to:

1. Add a sketch of the termination/soundness argument in the main body of the paper. This will require writing a new subsection, as the content in appendix is too detailed for this purpose.

2. Include a more detailed section on the evaluation of the feature, built from elements included in the present response.

3. Add a comparison of "bit-stealing made legal" in our Related Work section.

Reviewers A and C had interesting questions of the form "could <this performance trick from the C runtime> be presented in pure OCaml using your feature or an extension of it?". We will consider whether we want to say a few words about these in the paper, but may end up not being able to for reasons of space.


### Detail discussion

(Warning: some of those answers are a bit long, please feel free to skip any part of the content.)

#### Review A

> My only gripe is that the correctness proof of the algorithm, which I believe is one of the main contributions of the paper, is relegated completely to the appendix. If this is indeed a novel proof technique it would have been nice with the broad strokes described in the actual paper.

We had to make difficult choices for reasons of space, and focused on explaining the theorem statements and language design aspects, at the expense of discussing the implementation and showing the proofs. The chair announced that up to 3.5 extra pages would be available per paper, so we would be happy to include some proof sketches.

(Note: a natural cut point for the proof would be to define the termination measure, but then leave the proof that it is strictly decreasing out of the paper. This would correspond to including sections A.1 and A.2 of appendix A; but those are already too long so we would have some choices to make again.)

In terms of novelty: to the extent of our knowledge, the algorithm to decide termination appears to be novel -- our practical requirement on the cost of normalization checking may have led us in a new direction. This algorithm is presented in full in the paper proper. But the normalization proof of the algorithm, in appendix, follows a rather unsurprising approach of showing normalization of a rewrite system by providing a decreasing measure. It was not easy to find the right measure, but we would not describe it as a novel proof technique.


> *Section 1.4* My immediate reaction on reaching this section was "why not use a type system?". I think it would be well worth it to spend a section before this on discussing why you chose the normalisation approach instead of the type system approach. As it is you simply take it for granted that statically checking for confusion requires unfolding.

This does seem to deserve an explanation. We take for granted that computing precise head shapes correspond to normalization rather than typing, and we believe that a typing-based approach would either be too imprecise, or perform normalization under the hood.

If we consider the subset of datatype declarations that have at most one constructor per sum (possibly unboxed), then computing the head shape of a (closed) type corresponds exactly to computing the head normal form of the type seen as a simply-typed term -- the head constructor will be a primitive type former that determines the head shape. (With n-ary sums we have to ask to reduce sums strongly and other term constructors in head only: they are similar but harder to describe precisely.)

It is not clear how to define a type system where types correspond to the head constructor of terms (at least for closed terms). For example one could decide that the lambda-term `lambda x. foo x (bar x)` has type `_ -> foo` (any input is sent to an output with head constructor `foo`), and if all your first-order functions are "productive" in this way (their body is headed by a fixed term constructor) then you get a compositional type system. But how would you type `lambda x . x`, that is, the type declaration `type 'a id = 'a`? If you give it a "top" type approximation, you over-approximate the resulting shape (`int id` and `string id` have distinct shapes, yet you send them both to ⊤). If you add a form of polymorphism to your type system to give it a type of the form `forall x. x -> x`, then type-checking itself becomes an unfolding process.


> *l.510* Maybe worth noting that this optimisation for **lazy**, representing it as the underlying value is very similar to constructor unboxing. Is it a clear special case, or are there some differences?

This is similar, but not a special case. In our work, constructor unboxing is made safe by using a static analysis to guarantee that confusions will never happen -- if a type `'a t` uses unboxed constructors, we know that those constructors can be safely unboxed for any concrete instance of `'a`. The OCaml runtime on the contrary makes `lazy` representation decisions at runtime: if we viewed `'a lazy` as a sum type, the constructor `Forward(v)` would have a runtime representation that depends on the *value* of `v`, with a dynamic check to prevent confusions.

If this was written in OCaml (it is runtime code in C) with an imaginary extension of our proposal, it could be expressed as follows, assuming a `[@unboxed unsafe]` attribute that does not perform any static confusion check for this constructor. We intentionally do not provide this in our current design proposal, which focuses on safe uses. (We use the "imaginary `@tag` attribute" mentioned in Section 5.4.)
```ocaml
type 'a lazy = 'a lazy_state ref
type 'a lazy_state =
| Thunk of (unit -> 'a) [@tag lazy_tag]
| Forward of 'a         [@tag forward_tag]
| Forward_unboxed of 'a [@unboxed unsafe]

let make_forward (v : 'a) : 'a lazy_state =
  if List.mem (Obj.tag (Obj.repr v)) [Obj.lazy_tag; Obj.forward_tag; Obj.double_tag]
  then Forward v
  else Forward_unboxed v
```

One could even think of an imaginary `[@unboxed dynamic]` variant where the compiler is in charge of performing this dynamic check:

```ocaml
type 'a lazy_state =
| Thunk of (unit -> 'a) [@tag lazy_tag]
| Forward of 'a         [@tag forward_tag] [@unboxed dynamic]
```


> *l.510* I don't understand why you have to use ⊤ for `ty lazy`. Why can't you use $(∅, \{\mathrm{Lazy\_tag}, \mathrm{Forward\_tag}\}) ∪ \mathrm{headshape}(\mathit{ty})$?

This suggestion is correct, a nice refinement. Thanks! We missed it because we were set on treating `lazy` as a primitive type constructor, and on having the shape of primitive type constructors not depend on their arguments.


> *l.581* Note that this wouldn't just require allowing annotations on the type variables of data types, but anywhere you introduce type variables. For instance, when defining the map functions for your new option types. I guess this is part of what you are alluding to at l.595 as the non-trivial implementation changes, but it could be more explicit.

We have not worked on this ourselves, but we suspect that these shape restrictions on type variables could be inferred fairly well on top of Hindley-Damas-Milner type inference, so with work there would not necessarily be an extra annotation burden. (There is a large class of kind-like information on type variables that can be inferred well because they have a lattice structure: when creating a variable use the top layout, when unifying two variables take the meet of their kinds.)

> *l.640-641* I don't buy this it all. If you allow unboxing `Int` and `Bool` how would you compile the function
> ```
> let isInt (x : 'a t) : bool =
>   match x with
>   | Int(_) -> true
>   | Bool(_) -> false
> ```

Gasp. The reviewer is right and we now believe that this section is wrong. Thanks...


> *Section 7.5* Is the closed-higher-order language Turing complete? It looks like you could encode the SK combinators in it, but I'm unsure if the lack of partial application gets in the way.

This is a good question and we should clarify. One can consider the *untyped* closed-higher-order fragment, which indeed can directly express the SK combinators and is Turing-complete, and the *simply-typed* closed-higher-order fragment, where normalization is decidable -- as a restriction of the normalization result for the pure simply-typed lambda-calculus plus fixpoints.

When we say that C macros correspond to the closed-higher-order fragment, the C practitioners writing higher-order macros probably have the untyped system in mind. On the other hand, applications to higher-kinded type definitions would use the simply-typed system. Our algorithm does not depend on types, so our claims of soundness and *in*completeness apply to both the untyped and the typed system.


#### Review B

> First, I am not completely sold on the significance of the contributions, especially the parts around the approach to unboxing, which is very simple.

In future work, we are interested in going from the admittedly simple form of constructor unboxing to other data-representation transformations (we discuss several directions in the paper). One aspect of constructor unboxing that we believe is important in practice is its high-level nature: it does not rely on precise bit-level data representation of values, and there is hope of being able to provide performance improvements in a portable way across several backends with different datatype representation (native, javascript, wasm).

At the same time, we may need to understand the termination situation for higher-kinded types, if we want to extend the work to languages that have them.

> The on-the-fly termination checking algorithm---and its soundness and completeness proofs---seem quite significant, but it is not really the main focus of the paper.
> Or maybe one could argue that it is? I'm not sure.

The question of what contribution is more important is probably subjective, it depends on the reader and their interest. We tried to present both at best as we could, without deciding to focus on one at the expense of the other. The two questions are not independent:

- In a ML-family language without any form of unboxed types (including without single-constructor unboxing, which is already present in OCaml), the unfolding problem does not show up, as type synonyms are statically restricted to be acyclic. Principled unfolding with cycles is a scientific question that arises (for us) when we want to compute type-based information for optimization purposes, in presence of unboxed constructors. The scientific importance of our language design work comes in part from the deeper theoretical question that arise.

- Conversely, the specific problem of "on the fly" termination checking for the first-order lambda-calculus with recursion feels a bit arbitrary if it does not have an application. If one is only interested in the theoretical question of decidability, there are no reasons to require an on-the-fly solution. The importance of this theoretical question comes in part from the practical application it enables.

> Second, I find the evaluation of the approach a bit too limited.

In our experience, based for example on the previous integration of single-constructor unboxing in OCaml (or inline records), it is fairly difficult to evaluate this kind of performance-related language features experimentally. In particular, often the benefit in practice is not to be found in existing performance-sensitive codebases, because they are written with the current feature set in mind (they will write code *differently* to reach the same performance target).

The benefits generally come from:
- giving more, better options (safer, simpler, more idiomatic) to write performant code in the future
- reassuring users that a particular idiom they are using is "zero-cost" (even in cases where in fact the non-optimized approach would have had perfectly fine performance, there is a real productivity benefit for users to know that a given change has zero performance impact)

We focused our experimental evaluation on demonstrating that *some* performance benefits can be expected from the feature, by exhibiting Zarith as a use-case and trying to measure the performance benefits of unboxing for Zarith-bound programs.

> Only a single, specially-designed micro-benchmark is used for the Zarith case study, but it is not even described.

The benchmark code that we are running is the following:

```ocaml
let factorial of_int ( +! ) n =
  let rec ( *! ) a b =
    if b = 0 then of_int 0
    else a *! (b - 1) +! a
  in
  let rec fac n =
    if n = 0 then of_int 1
    else fac (n - 1) *! n
  in
  fac n
```

This defines a custom multiplication operator implemented using repeated additions, and then computes factorial using that: `fac n` performs a number of additions quadratic in `n`. We measured performance on various inputs, the 20% number comes from n=22; 63-bit integers overflow during the computation of n=21, so the choice n=22 has the property that there are O(n) operations that overflow and O(n^2) operations that do not overflow, representing some form of mixed workflow. On n=22, performing 10M iterations, the unboxed data definition takes 17s, the boxed data definition takes 22s... and the "wrong" version with short integers without any overflow check whatsoever takes 7s. Notice that the addition of overflow checks incurs a noticeably larger slowdown than the addition of boxing -- again, allocations in the minor GC are really fast.

We believe that the details of the benchmark really do not matter, the important aspect is that its performance should be representative of numeric workloads whose running time is entirely spent performing Zarith operations (adding unboxing can provide a 10-20% performance gain). An example of such workload are the Zarith-based implementations of 'pidigits' program computing the digits of pi by repeated rational approximations.  'pidigits' is included as a benchmark in the computer language benchmark game / shootout.

> Given that the authors when through the trouble of implementing their full approach in OCaml, I would have hoped to see more case-studies, and more performance results.

In the original RFC proposing unboxed constructor, Jeremy Yallop proposed the use-case of "ropes":

```ocaml
type rope = Leaf of string
          | Branch of { llen: int; l:rope; r:rope } [@@unboxed]
```

Jeremy Yallop implemented this type "manually" (unsafely) using unsafe casts, and provides a micro-benchmark (building large ropes out of single-character strings, and then concatenating them into a single string) where unboxing provides performance gains of up to 30%.

Another use-case where constructor unboxing could provide safety is the representation of Coq values in the `native_compute` implementation of compiled reduction. `native_compute` is a Coq tactic that compiles a Coq term into an OCaml term such that evaluating the OCaml term (by compilation then execution, in the usual call-by-value OCaml strategy) computes a strong normal form for the original Coq term. It uses an unsafe representation of values that mixes (unboxed) functions, sum constructors, and immediate values, and could be defined as a proper OCaml inductive if constructor unboxing was available. (This use-case is not our original idea either, it was suggested to us by Coq maintainers.)

Note: these aspects have been discussed publicly online, but providing a URL to those discussions for more details runs a high risk of de-anonymizing some of the authors of the present submission. In order to preserve double-blind we would not recommend looking for those example online, or reading in details the discussion that follows Jeremy Yallop's RFC (the RFC document iself is fine).

Finally, a use-case for this feature would be to make some form of dynamic introspection of runtime value more ergonomic than what is currently exposed in the Obj module. We could think of defining a sort of "universal type" as follows:

```ocaml
type dyn =
| Immediate of int [@unboxed]
| Block of dyn array [@unboxed]
| Float of float [@unboxed]
| String of string [@unboxed]
| Function of (dyn -> dyn) [@unboxed]
| Custom of custom [@unboxed]
| ...
and custom [@@shape [custom]]

let to_dyn : 'a -> dyn =
  fun x -> (Obj.magic x : dyn)
```

This interface cannot cover all needs -- for example it is not possible to distinguish between Int32, Int64 by their tags, they would be lumped together in the Custom case -- and we may need to restrict it due to shape approximations required by portability concerns. But it would still provide a pleasant pattern-matching interface for unsafe value-introspection code that people write today using the `Obj` module directly (a Github code search for Obj usage suggests https://github.com/rickyvetter/bucklescript/blob/cbc2bd65ce334e1fc83e1c4c5bf1468cfc15e7f9/jscomp/ext/ext_obj.ml#L25 for example).


> What are the main differences between your proposal and the one from Jeremy Yallop

In the details: Jeremy Yallop's proposal uses a global annotation [@@unboxed] on all constructors at once, we use a per-constructor annotation [@unboxed]. (The RFC mentions this as a possible extension in "Extension: partial unboxing".) A major difference is that Yallop's specification suggests renumbering constructors in some cases, where the representation of C of foo [@unboxed] is taken to be different from the representation of foo, in order to avoid conflicts with other constructors at this type. We do not support any such renumbering.

The big picture: one can think of the feature set that we implement as a "minimal valuable product" extracted from Jeremy Yallop's proposal: the simplest subset that is beneficial in practice. Independently, the user-facing interface is modified towards less magic and more expliciteness -- we are not in favor of having the compiler infer the representation.

We are interested in extending the feature set to provide more control over the value representation, but we feel that most of those extensions would be language-engineering contributions rather than research contributions.


> Could you propose a plan to improve the evaluation of this work?
> (Maybe, by adding unboxing annotations to existing projects, and running benchmarks on them?)

We are skeptical about the idea of adding unboxing annotations to existing projects and expecting noticeable performance wins. If the representation of a specific datatype is critical to the performance of a performance-sensitive project, then the authors quite probably have already made implementation choices that may be unsafe or less clear/maintainable to work around the lack of constructor unboxing. (This is not specific to constructor unboxing, in our experience most performance-oriented language features suffer from the same fate.)

Deep down, in terms of practical relevance, the claim we would like to make is "once upstreamed, this feature will be used by expert OCaml programmers and make them more productive". This is an extremely difficult claim to validate experimentally, and providing more detailed benchmarks is not clearly a good way to do it. (From our benchmark and Jeremy Yallop's we learned that expert users can reasonably expect 20-30% performance improvements for specific workloads that rely heavily on an unboxable variant.)

Probably a better sort of evidence would be the number of occurence of unsafe unboxing tricks (using Obj.magic or the C FFI) in real-world projects, including Zarith and Coq's `native_compute` machinery.

Note: reviewer A remarked that the Forward representation optimizations performed by the OCaml runtime are related to constructor unboxing. One could also take them as evidence of the performance impact of unboxing in general -- this use case falls outside the scope expressible with our proposed feature. The justification of those optimizations, introduced in the OCaml runtime by Damien Doligez in 2002, is that Andrew Tolmach was implementing search algorithms relying on lazy data structures in Haskell, and tried an OCaml port out of curiosity. The OCaml implementation was much slower, until the "forwarding" trick was introduced to make lazy structures more compact, giving performance results comparable to GHC at the time.


> In some cases, one would like more control about the memory layout than simple unboxing.
> For instance, if I define `type flags = { flag1 : bool; flag2 : bool; flag3 : bool }`, it is kind of wasteful to represent its inhabitants as a 0 block storing three full words, as the data can easily be encoded in bit-fields of a native integer.
> Could your work support that kind of optimization, or variations thereof?

Yes, one could easily design and implement on top of our work a "packing" optimization that turns products (tuples or immutable records) of finite immediate types into a single immediate types.

```ocaml
type flags = { flag1 : bool; flag2 : bool; flag3 : bool } [@@packed]
```

Our shape-computation logic would be called on each field of the record / component of the tuple, to check that they are finite immediates and to compute the shape of the packed type. (Currently our abstract domain of shapes stores possible immediate values in a finite set, here it would be much more efficient to have explicit support for "the immediates in [0; N]" or even "the immediates stored in W bits".)

Note that the question of portability shows up again. How many bits do we let users pack in an immediate value by default? In our experience with OCaml, these questions are very important for users in practice (there are surprisingly many reasons why some people still use 32bit-words or 32bit-integer systems, in particular Javascript runtimes).

In the long run, instead of piling bespoke attributes on top of each other, it may be more expressive to provide a DSL to describe low-level value representations, as done in Related Works on Hobbit, Dargent or in "Bit stealing made legal". This would be a major new direction, fairly orthogonal to the work we have done so far.



#### Review C


This paper defines unboxing as representing a constructor the same as its values. So even the first example (positive and negative integers) in the introduction, only one of Pos or Neg can be unboxed. Another way to define unboxing would be to avoid using a Block as the representation (i.e. avoid heap allocation). If the head shape of each constructor in this example included tag bits that were packed into a single word, this would be possible; but the tag bits mean that under your definition the value is not unboxed (because it has more information attached to it). This may be worth discussing?

I am not sure I follow the reviewer in the details here. Presumably the tag bits would be added on the `nat` payload of the `Pos` and `Neg` constructors (to replace the constructors). But `nat` may itself be either a block or an immediate, so the tagging strategy would depend on the value? This sounds doable, but also a headache. Maybe the reviewer has in mind something like this?

```ocaml
type posnat =
  | Zpos [@imm 0]
  | Spos of nat [@tag 0]

type negnat =
  | Zneg [@imm 1]
  | Sneg of nat [@tag 1]

type rel_num =
  | Pos of nat [@unboxed as posnat]
  | Neg of nat [@unboxed as negnat]
```

?

> This paper defines unboxing as representing a constructor the same as its values. So even the first example (positive and negative integers) in the introduction, only one of Pos or Neg can be unboxed. Another way to define unboxing would be to avoid using a Block as the representation (i.e. avoid heap allocation). If the head shape of each constructor in this example included tag bits that were packed into a single word, this would be possible; but the tag bits mean that under your definition the value is not unboxed (because it has more information attached to it). This may be worth discussing?

We am not sure that we follow the reviewer in the details here. Presumably the tag bits would be added on the `nat` payload of the `Pos` and `Neg` constructors (to replace the constructors). But `nat` may itself be either a block or an immediate, so the tagging strategy would depend on the value? This sounds doable, but also a headache. Maybe the reviewer has in mind something like this?

```ocaml
type posnat =
  | Zpos [@imm 0]
  | Spos of nat [@tag 0]

type negnat =
  | Zneg [@imm 1]
  | Sneg of nat [@tag 1]

type rel_num =
  | Pos of nat [@unboxed as posnat]
  | Neg of nat [@unboxed as negnat]
```

?


> From my reading, the checking of the unboxed annotation happens irrespective of any uses of a polymorphic type, i.e. must always hold? Thus the idea of adding an extension that constrains type parameter representations?

Yes. When checking the definition of (type 'a t = ...), it must not contain conflict for any possible value of ('a). On the other hand, when computing the head shape of (int t), we do use our knowledge of the parameter (int) to compute a precise shape -- the shape is computed on the definition of (t) with ('a) known to be int.


> Line 110: isn't it true that a definition of heads depends on both the language and its implementation (i.e. a specific target)?

The designer of the head shape system can choose either very precise head shapes, that are specific to one implementation of the language, or more coarse head shapes, that reject more unboxing annotation but are portable to more backends. In the case of OCaml, our current preference would go for defining a head shape that is a least common denominator between the native/bytecode, Javascript and wasm backends. (There is a tension between being able to guarantee more performance for users that know that they want to specialize on a given backend, and providing portable performance guarantees across implementations. One could think of interface hints for this, for example by distinguishing [@unboxed must] and [@unboxed if_possible].)


> Line 235: perhaps the way to say this is that it made more code safe by removing essentially a user-encoded tag switch and replacing it with a compiler-generated, safe one?

Our view is that the previous code used (correctly) features that, if misused, break type and memory soundness, while the new code uses features that may fail statically but always preserve type and memory soundness. (Caveat: any use of the FFI, and the use of the shape assumption on an abstract type, break this safety property.)

> Line 354: this formulation seems very tied to the polymorphic implementation strategy with a universal representation. Does this generalize?

Our intuition is that the formulation (taking the maximum of all closed instances) works for non-regular type representations as well. Consider for example a non-uniform variant of OCaml where `char array` is represented as a `string`, and `bool array` as a bitset. The head shape of the open type expression `'a array` is then any usual array, or a string, or a bitset. This does not depend on whether arrays, strings and bitsets are explicitly tagged in their runtime representation to be distinguishable from each other.

Of course, the reduction of dynamic type information in such non-uniform implementations can reduce unboxing opportunities, as `type 'a t = String of string [@unboxed] | Arr of 'a array [@unboxed]` is not accepted anymore.


> Line 437: I think this is also assuming a universal representation?

Making such an assumption was not our intention. On re-reading the whole section carefully, we made the mistake of defining constructor declarations as type components (line 345) as

    C of (tau1, tau2...)

without including their *return* type. This presentation implicitly assumes that a given constructor name will always have the same representation. Instead we should extend our `repr : Value -> Data` function to take type information `repr: Value -> Type -> Data`, and define our components with an explicit return type

    C : (tau1, tau2...) -> (tau1', ...) t

(Note that as long as the datatype declaration in our model remain in `C of (tau1, tau2...)` syntax, then this change does not introduce the full expressivity of GADTs, the closed type components obtained in this way will be instantiations of the non-generalized scheme

    C : (tau1...) -> (alpha1...) t

)

With this small fix, we hope that it is clear that we are not making universal representation assumptions. For example, our model of datatype declarations supports adding an `option` type where different instances `foo option` use different representations for the `None` and `Some` constructors -- then the head shape of `'a option` will be a union (maximum) of those possible representations.

Of course, our analysis (and our formal treatment of types with free type parameters) is meant to answer the question of "is `'a option` confusion-free *for any instance of `'a`", and this may not be the right question to ask for some languages. If the language uses agressive specialization of all polymorphic parameters (there is a synergy with non-uniform representations), it may be content with checking the absence of confusion for all monomorphic instances used in the program.

> Line 496: can all of these now be expressed as unboxing (perhaps with your extensions?)

For `lazy`, as we discussed in context of Review A, the answer is "not really" (or "you need even more extensions that we are not enthusiastic about"). For arrays, we could indeed consider having a data-structure defined in OCaml as

```ocaml
type 'a array =
| Addr of 'a generic_array [@unboxed]
| Float : floatarray -> floatarray [@unboxed]
```

`generic_array` would be an array tag that always has tag 0 and never tries to unbox floats. It not currently exist in the OCaml standard library (expect when enabling the non-standard `-no-flat-float-array` mode, then `'a array` behaves like this), although some users such as Coq maintainers have asked for it -- it can be implemented as a third-party library. `floatarray` is an existing name for the type of flat float arrays, available in the stdlib of recent-enough OCaml versions through the submodule `Float.Array`.

This would be enough to define several array-accessing functions in pure OCaml, but for array creation the runtime uses a dynamic test to say if its argument is a float; implementing it in OCaml would require exposing an extra (inelegant but safe) primitive to OCaml users:

```ocaml
val guess_if_float : 'a -> ('a, float) Type.eq option
```
