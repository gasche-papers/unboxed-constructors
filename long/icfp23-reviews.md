
Review #203A
===========================================================================

Overall merit
-------------
D. Reject

Reviewer expertise
------------------
X. Expert

Reviewer confidence
-------------------
3. Low

Paper summary
-------------
The authors present a compilation technique for specializing data constructors into primitives without identity while preserving the safety guarantees that users desire. The authors apply their methodology to a use case (an OSS library taken from the OCaml community/literature) and they present their termination algorithm that resembles to the CPP macro expansion termination strategy.

Comments for authors
--------------------
I would like to thank the authors for the effort they have put, but the paper is evidently still a work-in-progress, not yet up to ICFP standards.

I recognize that this paper has a lot of potential to improve the users' experience and increase safety guarantees and also draws parallels with CPP macro expansion (effectively and possibly formalizing it, I say possibly because the corresponding CPP section only gives hints). I enjoyed Section 5. Both the intuition and the formalization part. The paper describes a novel idea like the "head shapes" that combines nicely with pattern matching compilation strategy and GADTs (and not Guarded Algebraic Datatypes as stated in the abstract).

Unfortunately, reading it lowered significantly my confidence for it. It is written very informally in places. Some of the problems:

- Typos
- Grammatical errors
- Missing cross-references
- Dots "..." in various places
- Highly lacking related work, even not comparing with Jeremy's main "RFC
  Proposal: constructor unboxing"
- Highly lacking a comprehensive evaluation section (comprehensive microbenchmarking that involves various ADTs and pattern matching operations, possibly deeply nested to show the interactions with pattern matching compilation/optimization) and also macro-benchmarking:

Some comments starting from the abstract:

l6-7: "compiled away to just the identity": this is just an overloaded phrase.
What is an identity here? Identity on functions? On the underlying data?

l8: so this is an identity problem on the values. Unboxed values have also
structural identity (the content of memory), why that could lead to
"confusions"?

edit: Ah, in l96 we see that "confusions" is a term introduced in the paper.

l9-10: so the ability to sacrifice soundness for performance and vice-versa?
this sentence needs to be removed. It gives a wrong first impression.

l13: not Guarded Algebraic Datatypes (as I initially thought, the authors were combining this with https://www.normalesup.org/~simonet/publis/simonet-pottier-hmg-toplas.pdf) but I realize later that they meant GADT

After reading the abstract, I definitely understand what the authors worked
on--a way for users to annotate their programs with some kind of annotation that
will drive unboxing in a type driven way, without sacrificing correctness--.
However, the style is very informal and can be easily misunderstood.

l63: introduce -> introduce*s*

l67: the word "important" there is wrong. Important has usually a good
connotation. I think the authors mean "significant" or something else.

> However there are a few performance-critical situations where the overhead of
> this representation (compared to, say, carrying just a nat) is important

l70: "GHC will OCaml prefers", needs rewriting

l72: the authors could be more precise here. They show an attribute, not syntax

l83: inspired by design proposal -> a design proposal

l85: "allows unboxing one or several constructors of a sum type" -> grammar

l94: attributes -> attribute

l115: "several distinct values with the same runtime representation", no need to
repeat the definition here, it was defined a while before.

l116: "parametrized on two language specific, or even implementation-specific,
notions"

this comment makes it hard for the reader to conclude if that is a specification
detail or an implementation detail. There is a distinctive difference between
the two intentions, for compiler implementers.

l140: through an definition-unfolding -> a definition

l155-158: highly informal text

> (“precisely”: the cpp behavior is specified in prose that is difficult to
> understand, but now that we have proposed a precise formulation of our algorithm
> using inference rules, we believe that it is a reasonable formalization of the
> prose.)

There are a numerous points to be made about the contributions' list but the
most striking one is that the main motivation behind the paper (its even on the
title) is presented as merely "an unplanned side-effect" of the whole activity.

(at this point I will pause spell-checking comments-the paper needs to be
spell-checked/proofreading-check, in terms of its prose)

The authors present their system in 2.3 for the first time. While the `@unboxed`
is self-explanatory it would be ideal if the authors explain what is precisely
the "unsafe boundary". I get the intuition. Did the authors just use one synthetic
benchmark to compare their safe variant to unsafe Zarith?

The argument that the author's work compares to CPP macro expansion comes from
an uncited source in section 6. "As remarked by Stephen Dolan, our on-the-fly
termination checking algorithm [...]". This lowers the self-containment of
the paper significantly.

Regarding improving the writing style the authors can skim through related
papers, like the following, for inspiration:

    - Secrets of the Glasgow Haskell Compiler inliner
    https://www.microsoft.com/en-us/research/wp-content/uploads/2002/07/inline.pdf

    - Safe Zero-cost Coercions for Haskell
    https://www.seas.upenn.edu/~sweirich/papers/coercible-JFP.pdf

    - Levity polymorphism
    https://www.microsoft.com/en-us/research/wp-content/uploads/2016/11/levity-pldi17.pdf


* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *


Review #203B
===========================================================================

Overall merit
-------------
B. Weak accept

Reviewer expertise
------------------
Y. Knowledgeable

Reviewer confidence
-------------------
2. Medium

Paper summary
-------------
This paper is concerned with constructor unboxing, in particular it
devises a static analysis to decide whether an unboxing annotation is
safe to do, meaning it cannot be confused with another
constructor. Expanding the types to make the decision, corresponds to
a normalization problem for the unboxed-annotated constructor cases,
akin to CPP macro expansion - hence the title.

First the paper motivates the problem and provides relevant background
material, including a Zarith big-integer case study from OCaml. This
reports that replacing Zarith's existing unsafe code with safer fast
path code achieves the same performance, namely 20% faster than using
a safe, boxed representation.

The paper then builds up the prerequisites for a formalization:
- shapes of values
- head-shapes of type declarations (including polymorphic types),
  requiring multisets to account for all type variable instantiations,
- a representation function 'repr' over values,
- head-shape-syntax (an abstract domain really)
  incl. traditional (non-disjoint, total) union and partial disjoint
  union, and
- a big-step normalization relation, relating a type to its
  corresponding sum normal form.
This provides a high-level 2-step analysis specification for checking
unboxing annotations of a type:
 1. normalize the annotated type and
 2. check resulting sum-normal-forms for disjointness
At this point the paper describes the challenges met when scaling this
idea to full OCaml, explaining how the simpler formalized approach can
be made to work on lazy values, exceptions, GADTs, ...

Before the paper returns to formalism, it illustrates how the 'halting
problem' can be expressed as a reduction to a first-order
lambda-calculus normalization, known to be decidable. It goes on to
explain why repetition must be tracked on a per-subterm basis and then
goes into a formalization of an on-the-fly normalization algorithm.
This involves annotation terms with function name lists and
generalizing beta-reduction to tree-reductions over n-ary trees. A
termination argument is provided that builds up a well-founded
termination measure over multisets of multisets of node
constructors(!). The formalization concludes with a completeness
argument, expressing that any blocked reduction sequence, may be
turned into an erased, diverging sequence.

Finally the paper discusses the relation to the CPP proprocessor and
discusses related work.


This is in interesting paper - with lots of interesting material, both
compiler-wise (unboxing optimization and runtime representations) and
theoretical (the formalization, reductions, termination argument,
...), so a reader is in for quite a ride.

The described optimization is (as described) aimed at statically typed
languages with a uniform memory representation. I think the author(s)
argue convincingly for the relevance to other ML-family languages
beside OCaml.

For the benefit of a first-time reader, the text could perhaps state a
bit clearer the two-step decision process summarized above (normalize,
then check for disjointness).

A good chunk of the paper is dedicated to the formalization of the
normalization result. In itself this is described as a known
result. However I do understand that the general result may not be
practical for the current application, where only normalization of
annotated constructors is required.

It would be nice to see some experimental data indicating that this is
indeed practical. The submission describes the problems of extending
the basic idea into an implementation for full OCaml, but stops
there. In principle the described approach could be horribly slow in
terms of analysis/normalization time: It is hard to tell from the
current submission! Suggestions for experiments:
- Without any [@unboxed] annotations, does the OCaml compiler run
  without noticable slowdowns? Also for a self-compile?
- How much (compile) time did the safer variant of Zarith with
  [@unboxed] annotations take compared to the unsafe original?
- One can of course also imagine experiments on other/larger OCaml
  code bases, with and without added annotations.

In Sec.5.3 a term's annotations are described as lists without
duplicates. I'm wondering if annotating with a set would be slightly
simpler? The later ordering already uses super-set ordering among
sets.
z
The related work section is a bit thin. I was reminded of a few older
papers on data representation optimizations:
- Steenkiste's "The Implementation of Tags and Run-Time Type Checking"
  from "Topics in Advanced Language Implementation" 1991 and
- Henglein's "Global Tagging Optimization by Type Inference" from LFP1992.
None of these are however on constructor unboxing, but investigates
ways to optimize tagging and untagging operations.

Overall, the work is relevant to OCaml as well as many other similar
languages in the ICFP-family that enjoy a uniform value representation.


Details

l.437: Section ??

l.561: cass -> cases

l.665: detailed in ?

l.692: freee -> free

l.694: the the

l.823: (x <-- t_i) -> (x <-- \overline{t_i})
       to match l.817's description of the substitution

l.825: \alpha is free here

l.851: \overline{t} -> \lfloor \overline{t} \rfloor

l.899: i \in J \ {j} -> i \in I \ {j}

l.919: or or

l.949: Figure ??

l.956: The three -> The tree

l.989: Is the formal description correct? I can't follow the 'a' in
       Subtree(a)\{a} for instance.

l.1034: M O -> M(O)
l.1035z: M O -> M(O)

Questions for authors’ response
-------------------------------
- The 3rd tree expansion example in Fig.1 requires some than a single
  expansion step, doesn't it?


* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *


Review #203C
===========================================================================

Overall merit
-------------
D. Reject

Reviewer expertise
------------------
Y. Knowledgeable

Reviewer confidence
-------------------
1. High

Paper summary
-------------
In this paper, the authors propose a static analysis to determine whether the in-memory representation of some OCaml data constructor values can be spared a header corresponding to their data constructor. This static analysis ensures the injectivity of in-memory representation. Then, the authors implement this static analysis in a modified OCaml compiler, where users can provide annotations to decide which data constructors should be unboxed. This static analysis relies on deciding the termination of a fragment of first-order lambda calculus with recursion, which the authors purport to relate to the C preprocessor.

Comments for authors
--------------------
This paper is unevenly written.

+ Sections 1 to 4 are easy to follow, with a clear progression. The generalization of unboxed data constructors is a nice and useful feature to add to OCaml, and zarith is a convincingly motivating example.

- Section 6 on the C preprocessors is insufficient. It is not enough to "believe" that the C preprocessor (cpp) fits a particular element of the soundness proof of Section 5 to declare that Section 5 is a full proof of its termination. The authors should show this connection much more formally, beyond just saying "we believe." More globally, this paper leaves the impression that the work of Section 5 is half-baked: Section 1.5 suggests that the proof in Section 5 "may" give rise to an implementation, and the C preprocessor may very well be such an implementation, but Section 6 does not substantiate that claim.

* At the very least, I would have expected a formal algorithm describing the C preprocessor as an instantiation of the formalism of Section 5; on top of that, even just a proof-of-concept implementation would be welcome, to compare its results to some public cpp test suite (say, a subset of https://gcc.gnu.org/onlinedocs/gccint/C-Tests.html ) to check that the algorithm follows the C standard as described in Section 6 – and thus, that cpp indeed fits the formalism of Section 5. With the shortcomings of Section 6 as it is now, the title of this paper sounds like clickbait.

    * In fact, without implementation, I am not sure of the added value of Section 5 compared to previous proofs of the decidability of termination of order-1 recursive program schemes.

- The related work and references are quite light. Dargent is now published (POPL 2023: https://doi.org/10.1145/3571240 ) and its related work section should be a valuable source of inspiration for this submission here. Even from a higher-level point of view, there should be a more thorough discussion to compare the authors' approach with specialization approaches mentioned in Section 7, both in terms of expressiveness and in terms of compile-time and runtime performance.

For all these reasons, I lean towards rejection of this paper. At best, at this point, a venue more specific to the OCaml ecosystem could be more open to considering sections 1 to 4.

