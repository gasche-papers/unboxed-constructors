POPL 2024 Paper #252 Reviews and Comments
===========================================================================
Paper #252 Unboxed data constructors -- or, how cpp decides a halting
problem


Review #252A
===========================================================================

Overall merit
-------------
B. Weak accept: I lean towards acceptance.

Reviewer expertise
------------------
Y. Knowledgeable: I am knowledgeable about the topic of the paper (or at
   least key aspects of it).

Summary of the paper
--------------------
The paper describes a new language feature allowing algebraic data type constructors to be (selectively) unboxed, even in cases with multiple constructors. The key insight is that some types of values have distinguishable runtime representations (for instance immediate values and blocks in the case of OCaml). This is captured by a *head shape* abstraction and the question of whether a constructor can safely be unboxed is decided by checking that each constructor in a datatype has a distinct head shape.

Computing head shapes is done by normalising types, which leads to a problem with recursive types: how to ensure termination? The solution is a termination monitoring evaluation algorithm which is proved both sound and complete, effectively deciding the halting problem for the language in question (first order λ-calculus with recursion). It's observed that the algorithm is closely related to the one used by CPP to unfold macro expansions.

Assessment of the paper
-----------------------
This is a nice paper. It gives a clearly motivated and well presented account of a language feature that allows previously unsafe code to be written safely. The termination monitoring algorithm is elegant and relating it to the algorithm used by CPP, thus providing it with an indirect correctness proof is a nice bonus.

My only gripe is that the correctness proof of the algorithm, which I believe is one of the main contributions of the paper, is relegated completely to the appendix. If this is indeed a novel proof technique it would have been nice with the broad strokes described in the actual paper.

Detailed comments for authors
-----------------------------
*Section 1.4* My immediate reaction on reaching this section was "why not use a type system?". I think it would be well worth it to spend a section before this on discussing why you chose the normalisation approach instead of the type system approach. As it is you simply take it for granted that statically checking for confusion requires unfolding.

*l.130* Add a footnote mentioning that OCaml does not have higher kinds.

*l.316* boxed [constructors] and

*l.368* we define the head [of] an immediate

*l.501* uses [to] decide the "unboxed float record"

*l.510* Maybe worth noting that this optimisation for **lazy**, representing it as the underlying value is very similar to constructor unboxing. Is it a clear special case, or are there some differences?

*l.510* I don't understand why you have to use ⊤ for `ty lazy`. Why can't you use $(∅, \{\mathrm{Lazy\_tag}, \mathrm{Forward\_tag}\}) ∪ \mathrm{headshape}(\mathit{ty})$?

*l.560* that corresponds [to] our notion

*l.581* Note that this wouldn't just require allowing annotations on the type variables of data types, but anywhere you introduce type variables. For instance, when defining the map functions for your new option types. I guess this is part of what you are alluding to at l.595 as the non-trivial implementation changes, but it could be more explicit.

*l.640-641* I don't buy this it all. If you allow unboxing `Int` and `Bool` how would you compile the function
```
let isInt (x : 'a t) : bool =
  match x with
  | Int(_) -> true
  | Bool(_) -> false
```

*l.818* real-world OCaml program[s].

*l.873-874* Shouldn't it be the other way around? The body $t'$ is annotated, but the arguments not.

*l.963* Turner's paper on supercombinators is from 1979 not 1797!

*l.982* real-world use [of] the

*Section 7.5* Is the closed-higher-order language Turing complete? It looks like you could encode the SK combinators in it, but I'm unsure if the lack of partial application gets in the way.

*l.1208* There's some grammar missing in *"means that adds"*.



Review #252B
===========================================================================

Overall merit
-------------
B. Weak accept: I lean towards acceptance.

Reviewer expertise
------------------
X. Expert: I am an expert on the topic of the paper (or at least key
   aspects of it).

Summary of the paper
--------------------
The paper extends the OCaml compiler with the option to unbox user-selected variant constructors, provided that the resulting data layout is disjoint for distinct variants.
This is demonstrated using the zarith type, that represents unbounded integers as either a small OCaml 63-bit integer, or a pointer to a custom block with a GMP integer (C representation).
In that case, the contents of the two variants (an integer and a pointer) can always be distinguished by their least-significant bit, and so both variants can be unboxed.
This approach achieves the same efficiency as current zarith (which relies on the same layout, that can already be achieved in unsafe C FFI code), and is shown to be 20% faster than a version without unboxing on some benchmark.

To determine whether the user-provided unboxing annotations are sensible (i.e., do not introduce ambiguity in the data layout), the authors design a straight-forward static analysis on type declarations.
The sole difficulty with this analysis, is that it is a priori not guaranteed to terminate due to recursive types.
The authors identify this problem as being equivalent to deciding termination in the pure, first-order λ-calculus with recursive definitions, which is known to be decidable.
They argue that the existing algorithms for proving termination in that system are not suitable for the paper's static analysis, as they implement a global analysis that would be inefficient.
As a consequence, the authors design a new algorithm that does on-the-fly termination checking, at the same time as performing the static analysis.

Interestingly, this approach to termination checking turns out to be related to an algorithm designed by Dave Prosser to perform macro expansion in the C pre-processor.
The authors thus prove that Dave Prosser's algorithm for macro expansion always terminates on a first-order restriction of the macro system.
They also note that both their and Dave Prosser's algorithm are incomplete on the higher-order macro fragment.

Supplementary material includes the OCaml implementation, and a technical appendix with detailed proofs and more details on the relation with Dave Prosser's algorithm.

Assessment of the paper
-----------------------
Datatype unboxing is an important avenue for getting more performance out of OCaml programs when that matters, so this paper is particularly welcome in my opinion.
Overall, I find that this is a really good paper, in that it is very clear, well-written, and appropriately illustrated with plenty of examples.
Most of the paper, if not all, should be accessible to non-experts, since the general approach is quite simple.
The more technical bit---the approach to on-the-fly termination checking---is more involved, especially the soundness and completeness proofs, which are appropriately left out of the paper (but included in the appendix).

That being said, I have two concerns with the paper, that explain why I give it a B and not just a straight A.

First, I am not completely sold on the significance of the contributions, especially the parts around the approach to unboxing, which is very simple.
The on-the-fly termination checking algorithm---and its soundness and completeness proofs---seem quite significant, but it is not really the main focus of the paper.
Or maybe one could argue that it is? I'm not sure.

Second, I find the evaluation of the approach a bit too limited.
Only a single, specially-designed micro-benchmark is used for the Zarith case study, but it is not even described.
Given that the authors when through the trouble of implementing their full approach in OCaml, I would have hoped to see more case-studies, and more performance results.

Detailed comments for authors
-----------------------------
Here are a some detailed comments, mostly attached to specific line numbers.

1-2: I think the title should be capitalized, and similarly for subsection titles throughout the paper.

8: "confusions" -> "confusion" (occurs many times in the introduction)

11: "slow version or a fast" -> "slow version, or a fast"

14: "Guarded Algebraic Datatypes" are more often referred to as "Generalized Algebraic Datatypes", but maybe you can just use "GATDs" as later in the paper since anyone interested in this paper will know what they are.

55: "performance overhead" -> "a performance overhead"

63: In many places the text overflows in the margin: fix this.

64: "Remark that" -> "Note that"

72-78: while reading this paragraph, I immediately thought of Rust's niche-filling optimization: maybe say something about it here? (Or just point to the related work section?)

90: "clash_between_int_and_int" -> "clash" (will make the line fit)

92: I would expect "overlapping" to be aligned with "This" on the line above (but maybe that's just alignment OCD on my part)

136: "another community ... on the fly" -> "another example where non-termination must be detected on the fly."

142: Add a citation for Yallop's proposal.

143: "compiler and hope" -> "compiler, and hope"

144: "of construction" -> "of constructor"

148: "system and some" -> "system, and some"

158: "After doing this work" sounds weird, also "algorith" -> "algorithm"

168: "second one..." -> "second one, etc." (for consistency with the second item)

176: You capitalize OCaml tag names like "Custom_tag", whereas they are not capitalized at least in the Obj module
I would change everywhere to "custom_tag" and similar for other tags.

177: "the FFI." -> "the C FFI." maybe?

180: "native integers are 63-bit integers on 64-bit-word machines" -> "native integers are 63-bit on 64-bit machines"

185: "2.2  unsafe Zarith" -> "2.2  Unsafe Zarith"

197: "Zarith uses today" -> "At the time of writing, Zarith uses"

235: "(by the FFI)" -> "(via the FFI)"

247-249: Punctuation is weird in the last sentence.

253: "function" -> "functional"

259: "A datatype definition defines" -> "A datatype definition specifies"

261: "of arguments of arguments" -> "of arguments"

262: The "of τl" bit is missing in the parentheses.

263: Subscript i should be on τ (last τ of the line)

265: "custom values..." -> "custom values, etc."

271-277: You are making very heavy use of indices here, and this makes the rules kind of hard to parse.
I think it might be better to use syntax with ellipses.

312: The notation "C ∅" is a bit weird, I would just write "C" (several occurrences)

316: "consturctors" -> "constructors"

332: "cleanest" -> maybe "most satisfactory"?

352: You are missing the "ClosedTypes" subscript on "headshape(τ)" I believe.

416: You use "i" both (bound) as index in the LHS of the definition, and in the RHS, that may be confusing.

425: "do not duplicate polymorphic functions": not clear what you mean here, like with template polymorphism?

463: Subscripts missing on the RHS of the definition, on the two "headshape".

520-522: "This option is always available ... in their program." -> "This option is always available for types that we don't want to reason about until a convincing use-case is found."

534-535: "In this context, ... introduces a bug.": I cannot understand what this sentence means.

560: "corresponds from" -> "corresponds to"

569: "(Don't ask.)": that gave me a good laugh. :)

652: "t * int if t is abstract" -> "int * t if t is abstract"

653: "with each other type" -> "with other types"

697: "would have been to" -> "would be to"

705-706: "The set of ... on shapes.": I do not understand this sentence, especially at the start of a paragraph.

726: "we mention that" -> "we mentioned that"

741: "rephrased as lambda-term" -> "rephrased as a lambda-term" (I would also use "λ-" instead of "lambda-" everywhere)

825: "(in λ-calculus syntax)": you seem to have a very liberal view of λ-calculus syntax.

830: "The second idea is if detecting" -> "The second idea is that, if detecting"

880: "σ(α)" -> "σ(x)"

942: "parenthesis" -> "parentheses"

1013: "note the trace" -> "not the trace"

1208: "means that adds some" -> "means that it adds some"

Questions to be addressed by author response
--------------------------------------------
What are the main differences between your proposal and the one from Jeremy Yallop?

Could you propose a plan to improve the evaluation of this work?
(Maybe, by adding unboxing annotations to existing projects, and running benchmarks on them?)

The following question is more open-ended, so feel free to disregard it if you don't have space in your response.
In some cases, one would like more control about the memory layout than simple unboxing.
For instance, if I define `type flags = { flag1 : bool; flag2 : bool; flag3 : bool }`, it is kind of wasteful to represent its inhabitants as a 0 block storing three full words, as the data can easily be encoded in bit-fields of a native integer.
Could your work support that kind of optimization, or variations thereof?



Review #252C
===========================================================================

Overall merit
-------------
A. Strong accept: I will argue for acceptance.

Reviewer expertise
------------------
X. Expert: I am an expert on the topic of the paper (or at least key
   aspects of it).

Summary of the paper
--------------------
This paper introduces a new type of data representation optimization driven through unboxing annotations that can be applied to individual constructors of ADT declarations. Defining a formalism over representations that is based around {heads} and {headshapes}, a requisite static analysis for the feasibility of unboxed representations is described. The framework of heads and headshapes is generalized to allow instantiations with different possible representations, depending on the language and compiler.

Because type definitions can have type parameters, this results in a fundamental recursion on types that is equivalent to the first-order lambda calculus (i.e. applying type constructors to types unconditionally). The paper recognizes the fundamental connection to the representation analysis necessary to check whether constructors can be unboxed and provides an algorithm that can properly handle polymorphic recursion (even non-uniform recursion) and check termination (i.e. doesn't explode with too many specializations).

The "cpp" in the title of the paper refers to the C preprocessor, which faces a similar problem with applying potentially recursive macro definitions. The paper uncovered a connection nearly lost to time and documents both the cpp algorithm and their delta to the cpp algorithm. They also prove termination for their algorithm and an important subcase of Prosser's algorithm in cpp.

Assessment of the paper
-----------------------
This is a strong paper with a new approach to unboxing ADTs that is a clear advancement on the state of the art, both in Rust and OCaml. It provides a formalism that is powerful enough to describe many different kinds of unboxing strategies. The particular unboxing strategy that they demonstrate for OCaml allows unboxing in more situations than the default implementation and provides an understandable programming model: annotate a constructor with ```[@unboxed]```, and if it that fails, then there will be an error.

The paper uses a specific meaning of "unboxing": an unboxed data constructor with a value is represented exactly the same as the value it contains. This means that no tag is necessary, because the representation of all of the other constructors is distinguishable from the value without using a tag. Other notions of unboxing are possible, and a nice property is that the framework for computing and checking representations can accommodate some, perhaps most. The authors apply this framework successfully to extend their notion of unboxing in OCaml, which only previously did very ad hoc unboxing optimizations.

A key subproblem of computing representations is basically the expansion of polymorphic types. This boils down to recursively expanding them by applying a type constructor to type arguments, which of course, can diverge for programs with non-uniform polymorphic recursion. In a language where this is legal, then expansion needs to stop somewhere. This paper offers a termination-checking algorithm that turns out to be related to the algorithm for macro expansion in cpp. The formal treatment of both this paper's new algorithm and Prosser's prior algorithm is a solid positive contribution to the literature.

Detailed comments for authors
-----------------------------
This paper defines unboxing as representing a constructor the same as its values. So even the first example (positive and negative integers) in the introduction, only one of Pos or Neg can be unboxed. Another way to define unboxing would be to avoid using a Block as the representation (i.e. avoid heap allocation). If the head shape of each constructor in this example included tag bits that were packed into a single word, this would be possible; but the tag bits mean that under your definition the value is not unboxed (because it has more information attached to it). This may be worth discussing?

From my reading, the checking of the unboxed annotation happens irrespective of any uses of a polymorphic type, i.e. must always hold? Thus the idea of adding an extension that constrains type parameter representations?

Line 85: this might be a good place to introduce OCaml's tagging system

Line 100: a diagram of head shapes might be useful, e.g. showing how heads and headshape multisets relate

Line 110: isn't it true that a definition of heads depends on both the language and its implementation (i.e. a specific target)?

Line 166: it probably makes sense to also declare OCaml's universal data representation which it uses to implement polymorphism?

Line 235: perhaps the way to say this is that it made more code safe by removing essentially a user-encoded tag switch and replacing it with a compiler-generated, safe one?

Line 249: this is provided that the representations available are powerful enough to allow unboxing

Line 257: K is never explained properly. This must be the universe of tags?

Line 286: consider rewording this for readers that don't immediately recall what injective means

Line 293: This sentence is kind of circular; I think you want to say that this definition captures the fact that unboxed constructors are represented the same as their value.

Line 312: What is K here again?

Line 329: this implies that we cannot have data representations with unused bits

Line 335: this would be helpful as a bulleted list, considering it's important syntax

Line 339: do these two macros even need to exist? they could be inlined where used with probably no less readability

Line 354: this formulation seems very tied to the polymorphic implementation strategy with a universal representation. Does this generalize?

Line 384: top not explained yet

Line 395: is this overload of the matching function necessary?

Line 437: I think this is also assuming a universal representation?

Line 496: can all of these now be expressed as unboxing (perhaps with your extensions?)

Line 542: the intersection of value representations in these might not be that big

Line 557: this is actually a GREAT CONTRIBUTION and could be pulled up into intro

Line 569. Yeah.

Line 605: why not an explicit ```[@boxed]``` annotation to break recursion?

Line 667: does this explict need polymorphism? 

Line 674: this is basically manually pushing value representations apart

Line 690: this could potentially make an ADT non-referentially transparent if the boxing function is not pure

Line 717: but presumably these would be cached by default?

Line 1152: Please also compare to EverParse by Nikhil Swamy and MSR.
