> POPL 2024 Paper #252 Reviews and Comments
> ===========================================================================
> Paper #252 Unboxed data constructors -- or, how cpp decides a halting
> problem
> 
> 
> Review #252A
> ===========================================================================
> 
> Overall merit
> -------------
> B. Weak accept: I lean towards acceptance.
> 
> Reviewer expertise
> ------------------
> Y. Knowledgeable: I am knowledgeable about the topic of the paper (or at
>    least key aspects of it).
> 
> Summary of the paper
> --------------------
> The paper describes a new language feature allowing algebraic data type constructors to be (selectively) unboxed, even in cases with multiple constructors. The key insight is that some types of values have distinguishable runtime representations (for instance immediate values and blocks in the case of OCaml). This is captured by a *head shape* abstraction and the question of whether a constructor can safely be unboxed is decided by checking that each constructor in a datatype has a distinct head shape.
> 
> Computing head shapes is done by normalising types, which leads to a problem with recursive types: how to ensure termination? The solution is a termination monitoring evaluation algorithm which is proved both sound and complete, effectively deciding the halting problem for the language in question (first order λ-calculus with recursion). It's observed that the algorithm is closely related to the one used by CPP to unfold macro expansions.
> 
> Assessment of the paper
> -----------------------
> This is a nice paper. It gives a clearly motivated and well presented account of a language feature that allows previously unsafe code to be written safely. The termination monitoring algorithm is elegant and relating it to the algorithm used by CPP, thus providing it with an indirect correctness proof is a nice bonus.
> 
> My only gripe is that the correctness proof of the algorithm, which I believe is one of the main contributions of the paper, is relegated completely to the appendix. If this is indeed a novel proof technique it would have been nice with the broad strokes described in the actual paper.

DONE wrote a proof sketch outside the appendix

We had to make difficult choices for reasons of space, and focused on explaining the theorem statements and language design aspects, at the expense of discussing the implementation and showing the proofs. The chair announced that up to 3.5 extra pages would be available per paper, so we would be happy to include some proof sketches.

(Note: a natural cut point for the proof would be to define the termination measure, but then leave the proof that it is strictly decreasing out of the paper. This would correspond to including sections A.1 and A.2 of appendix A; but those are already too long so we would have some choices to make again.)

In terms of novelty: to the extent of our knowledge, the algorithm to decide termination appears to be novel -- our practical requirement on the cost of normalization checking may have led us in a new direction. This algorithm is presented in full in the paper proper. But the normalization proof of the algorithm, in appendix, follows a rather unsurprising approach of showing normalization of a rewrite system by providing a decreasing measure. It was not easy to find the right measure, but we would not describe it as a novel proof technique.


> Detailed comments for authors
> -----------------------------
> *Section 1.4* My immediate reaction on reaching this section was "why not use a type system?". I think it would be well worth it to spend a section before this on discussing why you chose the normalisation approach instead of the type system approach. As it is you simply take it for granted that statically checking for confusion requires unfolding.

NOT DONE I (Gabriel) decided against having this discussion

This does seem to deserve an explanation. We take for granted that computing precise head shapes correspond to normalization rather than typing, and we believe that a typing-based approach would either be too imprecise, or perform normalization under the hood.

If we consider the subset of datatype declarations that have at most one constructor per sum (possibly unboxed), then computing the head shape of a (closed) type corresponds exactly to computing the head normal form of the type seen as a simply-typed term -- the head constructor will be a primitive type former that determines the head shape. (With n-ary sums we have to ask to reduce sums strongly and other term constructors in head only: they are similar but harder to describe precisely.)

It is not clear how to define a type system where types correspond to the head constructor of terms (at least for closed terms). For example one could decide that the lambda-term `lambda x. foo x (bar x)` has type `_ -> foo` (any input is sent to an output with head constructor `foo`), and if all your first-order functions are "productive" in this way (their body is headed by a fixed term constructor) then you get a compositional type system. But how would you type `lambda x . x`, that is, the type declaration `type 'a id = 'a`? If you give it a "top" type approximation, you over-approximate the resulting shape (`int id` and `string id` have distinct shapes, yet you send them both to ⊤). If you add a form of polymorphism to your type system to give it a type of the form `forall x. x -> x`, then type-checking itself becomes an unfolding process.

> *l.130* Add a footnote mentioning that OCaml does not have higher kinds.

I added a more nuanced subsection about this in the "Scaling to a full
language" section.

> *l.316* boxed [constructors] and
>
> *l.368* we define the head [of] an immediate
> 
> *l.501* uses [to] decide the "unboxed float record"

all done

> *l.510* Maybe worth noting that this optimisation for **lazy**, representing it as the underlying value is very similar to constructor unboxing. Is it a clear special case, or are there some differences?

DONE discussed in the article

This is similar, but not a special case. In our work, constructor unboxing is made safe by using a static analysis to guarantee that confusions will never happen -- if a type `'a t` uses unboxed constructors, we know that those constructors can be safely unboxed for any concrete instance of `'a`. The OCaml runtime on the contrary makes `lazy` representation decisions at runtime: if we viewed `'a lazy` as a sum type, the constructor `Forward(v)` would have a runtime representation that depends on the *value* of `v`, with a dynamic check to prevent confusions.

If this was written in OCaml (it is runtime code in C) with an imaginary extension of our proposal, it could be expressed as follows, assuming a `[@unboxed unsafe]` attribute that does not perform any static confusion check for this constructor. We intentionally do not provide this in our current design proposal, which focuses on safe uses. (We use the "imaginary `@tag` attribute" mentioned in Section 5.4.)
```ocaml
type 'a lazy = 'a lazy_state ref
type 'a lazy_state =
| Thunk of (unit -> 'a) [@tag lazy_tag]
| Forward of 'a         [@tag forward_tag]
| Forward_unboxed of 'a [@unboxed unsafe]

let make_forward (v : 'a) : 'a lazy_state =
  if List.mem (Obj.tag (Obj.repr v)) [Obj.lazy_tag; Obj.forward_tag; Obj.double_tag]
  then Forward v
  else Forward_unboxed v
```

One could even think of an imaginary `[@unboxed dynamic]` variant where the compiler is in charge of performing this dynamic check:

```ocaml
type 'a lazy_state =
| Thunk of (unit -> 'a) [@tag lazy_tag]
| Forward of 'a         [@tag forward_tag] [@unboxed dynamic]
```

> *l.510* I don't understand why you have to use ⊤ for `ty lazy`. Why can't you use $(∅, \{\mathrm{Lazy\_tag}, \mathrm{Forward\_tag}\}) ∪ \mathrm{headshape}(\mathit{ty})$?

This suggestion is correct, a nice refinement. Thanks! We missed it because we were set on treating `lazy` as a primitive type constructor, and on having the shape of primitive type constructors not depend on their arguments.

done integrating in the paper

> *l.560* that corresponds [to] our notion
> 
> *l.581* Note that this wouldn't just require allowing annotations on the type variables of data types, but anywhere you introduce type variables. For instance, when defining the map functions for your new option types. I guess this is part of what you are alluding to at l.595 as the non-trivial implementation changes, but it could be more explicit.

We have not worked on this ourselves, but we suspect that these shape restrictions on type variables could be inferred fairly well on top of hindley-milner type inference, so with work there would not necessarily be an extra annotation burden. (There is a large class of kind-like information on type variables that can be inferred well because they have a lattice structure: when creating a variable use the top layout, when unifying two variables take the meet of their kinds.)

> *l.640-641* I don't buy this it all. If you allow unboxing `Int` and `Bool` how would you compile the function
> ```
> let isInt (x : 'a t) : bool =
>   match x with
>   | Int(_) -> true
>   | Bool(_) -> false
> ```

Gasp. The reviewer is right and I think that this section is entirely wrong.

> *l.818* real-world OCaml program[s].

done

> *l.873-874* Shouldn't it be the other way around? The body $t'$ is annotated, but the arguments not.

there is a confusion "t' is annotated" could mean either "t' is in the grammar of annotated terms" or "the expansion adds annotations to t' to make it annotated". What happens is that t' is a non-annotated term (and the arguments are annotated), and the substitution adds annotation to the subterms of t' that are not its free variable.

(I tried to reformulate slightly but I am not sure the result really avoids confusion.)

> *l.963* Turner's paper on supercombinators is from 1979 not 1797!

done

> *l.982* real-world use [of] the

done

> *Section 7.5* Is the closed-higher-order language Turing complete? It looks like you could encode the SK combinators in it, but I'm unsure if the lack of partial application gets in the way.

This is a good question and we should clarify. One can consider the *untyped* closed-higher-order fragment, which indeed can directly express the SK combinators and is Turing-complete, and the *simply-typed* closed-higher-order fragment, where normalization is decidable -- as a restriction of the normalization result for the pure simply-typed lambda-calculus plus fixpoints.

When we say that C macros correspond to the closed-higher-order fragment, the C practitioners writing higher-order macros probably have the untyped system in mind. On the other hand, applications to higher-kinded type definitions would use the simply-typed system. Our algorithm does not depend on types, so our claims of soundness and *in*completeness apply to both the untyped and the typed system.

DONE clarify in the paper

> *l.1208* There's some grammar missing in *"means that adds"*.

done

> 
> 
> Review #252B
> ===========================================================================
> 
> Overall merit
> -------------
> B. Weak accept: I lean towards acceptance.
> 
> Reviewer expertise
> ------------------
> X. Expert: I am an expert on the topic of the paper (or at least key
>    aspects of it).
> 
> Summary of the paper
> --------------------
> The paper extends the OCaml compiler with the option to unbox user-selected variant constructors, provided that the resulting data layout is disjoint for distinct variants.
> This is demonstrated using the zarith type, that represents unbounded integers as either a small OCaml 63-bit integer, or a pointer to a custom block with a GMP integer (C representation).
> In that case, the contents of the two variants (an integer and a pointer) can always be distinguished by their least-significant bit, and so both variants can be unboxed.
> This approach achieves the same efficiency as current zarith (which relies on the same layout, that can already be achieved in unsafe C FFI code), and is shown to be 20% faster than a version without unboxing on some benchmark.
> 
> To determine whether the user-provided unboxing annotations are sensible (i.e., do not introduce ambiguity in the data layout), the authors design a straight-forward static analysis on type declarations.
> The sole difficulty with this analysis, is that it is a priori not guaranteed to terminate due to recursive types.
> The authors identify this problem as being equivalent to deciding termination in the pure, first-order λ-calculus with recursive definitions, which is known to be decidable.
> They argue that the existing algorithms for proving termination in that system are not suitable for the paper's static analysis, as they implement a global analysis that would be inefficient.
> As a consequence, the authors design a new algorithm that does on-the-fly termination checking, at the same time as performing the static analysis.
> 
> Interestingly, this approach to termination checking turns out to be related to an algorithm designed by Dave Prosser to perform macro expansion in the C pre-processor.
> The authors thus prove that Dave Prosser's algorithm for macro expansion always terminates on a first-order restriction of the macro system.
> They also note that both their and Dave Prosser's algorithm are incomplete on the higher-order macro fragment.
> 
> Supplementary material includes the OCaml implementation, and a technical appendix with detailed proofs and more details on the relation with Dave Prosser's algorithm.
> 
> Assessment of the paper
> -----------------------
> Datatype unboxing is an important avenue for getting more performance out of OCaml programs when that matters, so this paper is particularly welcome in my opinion.
> Overall, I find that this is a really good paper, in that it is very clear, well-written, and appropriately illustrated with plenty of examples.
> Most of the paper, if not all, should be accessible to non-experts, since the general approach is quite simple.
> The more technical bit---the approach to on-the-fly termination checking---is more involved, especially the soundness and completeness proofs, which are appropriately left out of the paper (but included in the appendix).
> 
> That being said, I have two concerns with the paper, that explain why I give it a B and not just a straight A.
> 
> First, I am not completely sold on the significance of the contributions, especially the parts around the approach to unboxing, which is very simple.

In future work, we are interested in going from the admittedly simple form of constructor unboxing to other data-representation transformations (we discuss several directions in the paper). One aspect of constructor unboxing that we believe is important in practice is its high-level nature: it does not rely on precise bit-level data representation of values, and there is hope of being able to provide performance improvements in a portable way across several backends with different datatype representation (native, javascript, wasm).

At the same time, we may need to understand the termination situation for higher-kinded types, if we want to extend the work to languages that have them.

> The on-the-fly termination checking algorithm---and its soundness and completeness proofs---seem quite significant, but it is not really the main focus of the paper.
> Or maybe one could argue that it is? I'm not sure.

The question of what contribution is more important is probably subjective, it depends on the reader and their interest. We tried to present both at best as we could, without deciding to focus on one at the expense of the other. The two questions are not independent:

- In a ML-family language without any form of unboxed types (including without single-constructor unboxing, which is already present in OCaml), the unfolding problem does not show up, as type synonyms are statically restricted to be acyclic. Principled unfolding with cycles is a scientific question that arises (for us) when we want to compute type-based information for optimization purposes, in presence of unboxed constructors. The scientific importance of our language design work comes in part from the deeper theoretical question that arise.

- Conversely, the specific problem of "on the fly" termination checking for the first-order lambda-calculus with recursion feels a bit arbitrary if it does not have an application. If one is only interested in the theoretical question of decidability, there are no reasons to require an on-the-fly solution. The importance of this theoretical question comes in part from the practical application it enables.

> Second, I find the evaluation of the approach a bit too limited.

DONE integrated in the article

In our experience, based for example on the previous integration of single-constructor unboxing in OCaml (or inline records), it is fairly difficult to evaluate this kind of performance-related language features experimentally. In particular, often the benefit in practice is not to be found in existing performance-sensitive codebases, because they are written with the current feature set in mind (they will write code *differently* to reach the same performance target).

The benefits generally come from:
- giving more, better options (safer, simpler, more idiomatic) to write performant code in the future
- reassuring users that a particular idiom they are using is "zero-cost" (even in cases where in fact the non-optimized approach would have had perfectly fine performance, there is a real productivity benefit for users to know that a given change has zero performance impact)

We focused our experimental evaluation on demonstrating that *some* performance benefits can be expected from the feature, by exhibiting Zarith as a use-case and trying to measure the performance benefits of unboxing for Zarith-bound programs.

> Only a single, specially-designed micro-benchmark is used for the Zarith case study, but it is not even described.

The benchmark code that we are running is the following:

```ocaml
let factorial of_int ( +! ) n =
  let rec ( *! ) a b =
    if b = 0 then of_int 0
    else a *! (b - 1) +! a
  in
  let rec fac n =
    if n = 0 then of_int 1
    else fac (n - 1) *! n
  in
  fac n
```

This defines a custom multiplication operator implemented using repeated additions, and then computes factorial using that: `fac n` performs a number of additions quadratic in `n`. We measured performance on various inputs, the 20% number comes from n=22; 63-bit integers overflow during the computation of n=21, so the choice n=22 has the property that there are O(n) operations that overflow and O(n^2) operations that do not overflow, representing some form of mixed workflow. On n=22, performing 10M iterations, the unboxed data definition takes 17s, the boxed data definition takes 22s... and the "wrong" version with short integers without any overflow check whatsoever takes 7s. Notice that the addition of overflow checks incurs a noticeably larger slowdown than the addition of boxing -- again, allocations in the minor GC are really fast.

We believe that the details of the benchmark really do not matter, the important aspect is that its performance should be representative of numeric workloads whose running time is entirely spent performing Zarith operations (adding unboxing can provide a 10-20% performance gain). An example of such workload are the Zarith-based implementations of 'pidigits' program computing the digits of pi by repeated rational approximations.  'pidigits' is included as a benchmark in the computer language benchmark game / shootout.

> Given that the authors when through the trouble of implementing their full approach in OCaml, I would have hoped to see more case-studies, and more performance results.

In the original RFC proposing unboxed constructor, Jeremy Yallop proposed the use-case of "ropes":

```ocaml
type rope = Leaf of string [@unboxed]
          | Branch of { llen: int; l:rope; r:rope }
```

Jeremy Yallop implemented this type "manually" (unsafely) using unsafe casts, and provides a micro-benchmark (building large ropes out of single-character strings, and then concatenating them into a single string) where unboxing provides performance gains of up to 30%.

Another use-case where constructor unboxing could provide safety is the representation of Coq values in the `native_compute` implementation of compiled reduction. `native_compute` is a Coq tactic that compiles a Coq term into an OCaml term such that evaluating the OCaml term (by compilation then execution, in the usual call-by-value OCaml strategy) computes a strong normal form for the original Coq term. It uses an unsafe representation of values that mixes (unboxed) functions, sum constructors, and immediate values, and could be defined as a proper OCaml inductive if constructor unboxing was available. (This use-case is not our original idea either, it was suggested to us by Coq maintainers.)

Note: these aspects have been discussed publicly online, but providing a URL to those discussions for more details runs a high risk of de-anonymizing some of the authors of the present submission. In order to preserve double-blind we would not recommend looking for those example online, or reading in details the discussion that follows Jeremy Yallop's RFC (the RFC document iself is fine).

Finally, a use-case for this feature would be to make some form of dynamic introspection of runtime value more ergonomic than what is currently exposed in the Obj module. We could think of defining a sort of "universal type" as follows:

```ocaml
type dyn =
| Immediate of int [@unboxed]
| Block of dyn array [@unboxed]
| Float of float [@unboxed]
| String of string [@unboxed]
| Function of (dyn -> dyn) [@unboxed]
| Custom of custom [@unboxed]
| ...
and custom [@@shape [custom]]

let to_dyn : 'a -> dyn =
  fun x -> (Obj.magic x : dyn)
```

This interface cannot cover all needs -- for example it is not possible to distinguish between Int32, Int64 by their tags, they would be lumped together in the Custom case -- and we may need to restrict it due to shape approximations required by portability concerns. But it would still provide a pleasant pattern-matching interface for unsafe value-introspection code that people write today using the `Obj` module directly (a Github code search for Obj usage suggests https://github.com/rickyvetter/bucklescript/blob/cbc2bd65ce334e1fc83e1c4c5bf1468cfc15e7f9/jscomp/ext/ext_obj.ml#L25 for example).

> Detailed comments for authors
> -----------------------------
> Here are a some detailed comments, mostly attached to specific line numbers.
> 
> 1-2: I think the title should be capitalized, and similarly for subsection titles throughout the paper.

I (Gabriel) don't like capitalized titles, so I am going to not do it if I can.

> 8: "confusions" -> "confusion" (occurs many times in the introduction)

done

> 11: "slow version or a fast" -> "slow version, or a fast"

I wonder why people are obsessed with comma-placement rules in English. Both versions are just fine?

> 14: "Guarded Algebraic Datatypes" are more often referred to as "Generalized Algebraic Datatypes", but maybe you can just use "GATDs" as later in the paper since anyone interested in this paper will know what they are.

no change required

> 55: "performance overhead" -> "a performance overhead"

done

> 63: In many places the text overflows in the margin: fix this.

TODO; we will have to do this at some point anyway, and we have to
take this into account to compute the "real" page count and not add
too much in our revisions.

> 64: "Remark that" -> "Note that"

done

> 72-78: while reading this paragraph, I immediately thought of Rust's niche-filling optimization: maybe say something about it here? (Or just point to the related work section?)

I (Gabriel) am not sure what to do here. I find that this class of (perfectly reasonable) comments from the reviewers ("What you wrote at X made me think of Y and I would have liked to see Y acknowledged here rather than later in the paper") tends to generate worse writing, full of small interjections that disrupt the reading flow without actually being relevant for most readers.

I started adding a parenthesis saying "(Other languages implement related features, see our Related Work section \ref{...}.)" But this should go without saying, it is a pure waste of space and reader time.

If you wonder at this point whether we thought of Rust niche-filling rules, you can use the "search" feature of your PDF reader and you will find our discussion of Rust. I propose to leave it at that.

On the other hand, I propose to add a summary of the main points of our Related Work section in the introduction.

done: "Contributions" is now "Contributions and outline" and contains a summary of the Related Work content.

> 90: "clash_between_int_and_int" -> "clash" (will make the line fit)

done

> 92: I would expect "overlapping" to be aligned with "This" on the line above (but maybe that's just alignment OCD on my part)

The compiler aligns properly, I messed up when mixing this with color commands in the listings source. Fixed.

> 136: "another community ... on the fly" -> "another example where non-termination must be detected on the fly."

done

> 142: Add a citation for Yallop's proposal.

done

> 143: "compiler and hope" -> "compiler, and hope"

done (let's accept to add some extra commas to please)

> 144: "of construction" -> "of constructor"

removed

> 148: "system and some" -> "system, and some"

removed

> 158: "After doing this work" sounds weird, also "algorith" -> "algorithm"

done

> 168: "second one..." -> "second one, etc." (for consistency with the second item)

done

> 176: You capitalize OCaml tag names like "Custom_tag", whereas they are not capitalized at least in the Obj module
> I would change everywhere to "custom_tag" and similar for other tags.

The capitalized name is the name of the constant in the
C runtime. I (Gabriel) prefer to use the capitalized names which
I find more distinctive, we encounter them more often when hacking on
the compiler codebase, and they look at bit like data constructors
which is relevant to this work.

> 177: "the FFI." -> "the C FFI." maybe?

done, with an expansion of the acronym for non-experts

> 180: "native integers are 63-bit integers on 64-bit-word machines" -> "native integers are 63-bit on 64-bit machines"

done ("native integers" => "OCaml integers")

> 185: "2.2  unsafe Zarith" -> "2.2  Unsafe Zarith"

done

> 197: "Zarith uses today" -> "At the time of writing, Zarith uses"

"today" removed

> 235: "(by the FFI)" -> "(via the FFI)"

done

> 247-249: Punctuation is weird in the last sentence.

rewritten

> 253: "function" -> "functional"

done

> 259: "A datatype definition defines" -> "A datatype definition specifies"

In OCaml we distinguish "definitions" (in structures) and
"declarations" (in signatures), and "specifies" reads more like
a declaration than a definition to me. I guess that the reviewer finds
the repetition "definition defines" unpleasant. I replaced "defines"
by "introduces".

> 261: "of arguments of arguments" -> "of arguments"

done

> 262: The "of τl" bit is missing in the parentheses.

done

> 263: Subscript i should be on τ (last τ of the line)

ah, good catch

> 265: "custom values..." -> "custom values, etc."

I get it, I get it, we have decided to only ever use "etc." for non-exhaustive enumerations. Okay.

> 271-277: You are making very heavy use of indices here, and this makes the rules kind of hard to parse.
> I think it might be better to use syntax with ellipses.

For now I propose to stick with indices. Indices let us rigourously
deal with n-ary cases. I can remove the explicit $K$ (or in fact
$K_j$) domains to be more lightweight.

> 312: The notation "C ∅" is a bit weird, I would just write "C" (several occurrences)

C \emptyset is more rigorous, I would rather keep it for now rather than introducing syntactic sugar that other readers might find confusing.

> 316: "consturctors" -> "constructors"

done

> 332: "cleanest" -> maybe "most satisfactory"?

I wonder what is wrong with "cleanest".

> 352: You are missing the "ClosedTypes" subscript on "headshape(τ)" I believe.

Good catch!

> 416: You use "i" both (bound) as index in the LHS of the definition, and in the RHS, that may be confusing.

Good catch. We typically use $j$ for boxed constructors and $k$ for their arguments. I find $i$ more readable here (and we are not ranging over all constructors, but over all {constant,non-constant} constructors, so it is not the same $j$ as in the type declaration). We already us $k$ for arguments in the "repr" function earlier.

> 425: "do not duplicate polymorphic functions": not clear what you mean here, like with template polymorphism?

The writing of this paragraph is in fact slightly contradictory with
the handling of lazy values suggested by review A. I removed this part
of the paragraph, and adding a note in the Lazy discussion.

> 463: Subscripts missing on the RHS of the definition, on the two "headshape".

done

> 520-522: "This option is always available ... in their program." -> "This option is always available for types that we don't want to reason about until a convincing use-case is found."

meh

> 534-535: "In this context, ... introduces a bug.": I cannot understand what this sentence means.

Rephrased: "In this context, silently ignoring representation requests
is arguably a bug: it breaks the specification the user has in mind."

> 560: "corresponds from" -> "corresponds to"

done

> 569: "(Don't ask.)": that gave me a good laugh. :)
> 
> 652: "t * int if t is abstract" -> "int * t if t is abstract"

this section was flawed and is removed

> 653: "with each other type" -> "with other types"

removed

> 697: "would have been to" -> "would be to"

done

> 705-706: "The set of ... on shapes.": I do not understand this sentence, especially at the start of a paragraph.

rewritten

> 726: "we mention that" -> "we mentioned that"

I usually use a general present tense to discuss the content of the document, and in particular avoid the future form for forward references.

> 741: "rephrased as lambda-term" -> "rephrased as a lambda-term" (I would also use "λ-" instead of "lambda-" everywhere)

done

> 825: "(in λ-calculus syntax)": you seem to have a very liberal view of λ-calculus syntax.

Sure! This was in opposition to "in ML datatype declaration syntax",
but I am happy to take credit for a liberal usage of a `let rec`
term-former.

> 830: "The second idea is if detecting" -> "The second idea is that, if detecting"

done

> 880: "σ(α)" -> "σ(x)"

good catch

> 942: "parenthesis" -> "parentheses"

done

> 1013: "note the trace" -> "not the trace"

done

> 1208: "means that adds some" -> "means that it adds some"

rewritten

> Questions to be addressed by author response
> --------------------------------------------
> What are the main differences between your proposal and the one from Jeremy Yallop

In the details: Jeremy Yallop's proposal uses a global annotation [@@unboxed] on all constructors at once, we use a per-constructor annotation [@unboxed]. (The RFC mentions this as a possible extension in "Extension: partial unboxing".) A major difference is that Yallop's specification suggests renumbering constructors in some cases, where the representation of C of foo [@unboxed] is taken to be different from the representation of foo, in order to avoid conflicts with other constructors at this type. We do not support any such renumbering.

The big picture: one can think of the feature set that we implement as a "minimal valuable product" extracted from Jeremy Yallop's proposal: the simplest subset that is beneficial in practice. Independently, the user-facing interface is modified towards less magic and more expliciteness -- we are not in favor of having the compiler infer the representation.

We are interested in extending the feature set to provide more control over the value representation, but we feel that most of those extensions would be language-engineering contributions rather than research contributions.

> Could you propose a plan to improve the evaluation of this work?
> (Maybe, by adding unboxing annotations to existing projects, and running benchmarks on them?)

We are skeptical about the idea of adding unboxing annotations to existing projects and expecting noticeable performance wins. If the representation of a specific datatype is critical to the performance of a performance-sensitive project, then the authors quite probably have already made implementation choices that may be unsafe or less clear/maintainable to work around the lack of constructor unboxing. (This is not specific to constructor unboxing, in our experience most performance-oriented language features suffer from the same fate.)

Deep down, in terms of practical relevance, the claim we would like to make is "once upstreamed, this feature will be used by expert OCaml programmers and make them more productive". This is an extremely difficult claim to validate experimentally, and providing more detailed benchmarks is not clearly a good way to do it. (From our benchmark and Jeremy Yallop's we learned that expert users can reasonably expect 20-30% performance improvements for specific workloads that rely heavily on an unboxable variant.)

Probably a better sort of evidence would be the number of occurence of unsafe unboxing tricks (using Obj.magic or the C FFI) in real-world projects, including Zarith and Coq's `native_compute` machinery.

Note: reviewer A remarked that the Forward representation optimizations performed by the OCaml runtime are related to constructor unboxing. One could also take them as evidence of the performance impact of unboxing in general -- this use case falls outside the scope expressible with our proposed feature. The justification of those optimizations, introduced in the OCaml runtime by Damien Doligez in 2002, is that Andrew Tolmach was implementing search algorithms relying on lazy data structures in Haskell, and tried an OCaml port out of curiosity. The OCaml implementation was much slower, until the "forwarding" trick was introduced to make lazy structures more compact, giving performance results comparable to GHC at the time.

> The following question is more open-ended, so feel free to disregard it if you don't have space in your response.
> In some cases, one would like more control about the memory layout than simple unboxing.
> For instance, if I define `type flags = { flag1 : bool; flag2 : bool; flag3 : bool }`, it is kind of wasteful to represent its inhabitants as a 0 block storing three full words, as the data can easily be encoded in bit-fields of a native integer.
> Could your work support that kind of optimization, or variations thereof?

Yes, one could easily design and implement on top of our work a "packing" optimization that turns products (tuples or immutable records) of finite immediate types into a single immediate types.

```ocaml
type flags = { flag1 : bool; flag2 : bool; flag3 : bool } [@@packed]
```

Our shape-computation logic would be called on each field of the record / component of the tuple, to check that they are finite immediates and to compute the shape of the packed type. (Currently our abstract domain of shapes stores possible immediate values in a finite set, here it would be much more efficient to have explicit support for "the immediates in [0; N]" or even "the immediates stored in W bits".)

Note that the question of portability shows up again. How many bits do we let users pack in an immediate value by default? In our experience with OCaml, these questions are very important for users in practice (there are surprisingly many reasons why some people still use 32bit-words or 32bit-integer systems, in particular Javascript runtimes).

In the long run, instead of piling bespoke attributes on top of each other, it may be more expressive to provide a DSL to describe low-level value representations, as done in Related Works on Hobbit, Dargent or in "Bit stealing made legal". This would be a major new direction, fairly orthogonal to the work we have done so far.

> Review #252C
> ===========================================================================
> 
> Overall merit
> -------------
> A. Strong accept: I will argue for acceptance.
> 
> Reviewer expertise
> ------------------
> X. Expert: I am an expert on the topic of the paper (or at least key
>    aspects of it).
> 
> Summary of the paper
> --------------------
> This paper introduces a new type of data representation optimization driven through unboxing annotations that can be applied to individual constructors of ADT declarations. Defining a formalism over representations that is based around {heads} and {headshapes}, a requisite static analysis for the feasibility of unboxed representations is described. The framework of heads and headshapes is generalized to allow instantiations with different possible representations, depending on the language and compiler.
> 
> Because type definitions can have type parameters, this results in a fundamental recursion on types that is equivalent to the first-order lambda calculus (i.e. applying type constructors to types unconditionally). The paper recognizes the fundamental connection to the representation analysis necessary to check whether constructors can be unboxed and provides an algorithm that can properly handle polymorphic recursion (even non-uniform recursion) and check termination (i.e. doesn't explode with too many specializations).
> 
> The "cpp" in the title of the paper refers to the C preprocessor, which faces a similar problem with applying potentially recursive macro definitions. The paper uncovered a connection nearly lost to time and documents both the cpp algorithm and their delta to the cpp algorithm. They also prove termination for their algorithm and an important subcase of Prosser's algorithm in cpp.
> 
> Assessment of the paper
> -----------------------
> This is a strong paper with a new approach to unboxing ADTs that is a clear advancement on the state of the art, both in Rust and OCaml. It provides a formalism that is powerful enough to describe many different kinds of unboxing strategies. The particular unboxing strategy that they demonstrate for OCaml allows unboxing in more situations than the default implementation and provides an understandable programming model: annotate a constructor with ```[@unboxed]```, and if it that fails, then there will be an error.
> 
> The paper uses a specific meaning of "unboxing": an unboxed data constructor with a value is represented exactly the same as the value it contains. This means that no tag is necessary, because the representation of all of the other constructors is distinguishable from the value without using a tag. Other notions of unboxing are possible, and a nice property is that the framework for computing and checking representations can accommodate some, perhaps most. The authors apply this framework successfully to extend their notion of unboxing in OCaml, which only previously did very ad hoc unboxing optimizations.
> 
> A key subproblem of computing representations is basically the expansion of polymorphic types. This boils down to recursively expanding them by applying a type constructor to type arguments, which of course, can diverge for programs with non-uniform polymorphic recursion. In a language where this is legal, then expansion needs to stop somewhere. This paper offers a termination-checking algorithm that turns out to be related to the algorithm for macro expansion in cpp. The formal treatment of both this paper's new algorithm and Prosser's prior algorithm is a solid positive contribution to the literature.
> 
> Detailed comments for authors
> -----------------------------
> This paper defines unboxing as representing a constructor the same as its values. So even the first example (positive and negative integers) in the introduction, only one of Pos or Neg can be unboxed. Another way to define unboxing would be to avoid using a Block as the representation (i.e. avoid heap allocation). If the head shape of each constructor in this example included tag bits that were packed into a single word, this would be possible; but the tag bits mean that under your definition the value is not unboxed (because it has more information attached to it). This may be worth discussing?

I am not sure I follow the reviewer in the details here. Presumably the tag bits would be added on the `nat` payload of the `Pos` and `Neg` constructors (to replace the constructors). But `nat` may itself be either a block or an immediate, so the tagging strategy would depend on the value? This sounds doable, but also a headache. Maybe the reviewer has in mind something like this?

```ocaml
type posnat =
  | Zpos [@imm 0]
  | Spos of nat [@tag 0]

type negnat =
  | Zneg [@imm 1]
  | Sneg of nat [@tag 1]

type rel_num =
  | Pos of nat [@unboxed as posnat]
  | Neg of nat [@unboxed as negnat]
```

?

> From my reading, the checking of the unboxed annotation happens irrespective of any uses of a polymorphic type, i.e. must always hold? Thus the idea of adding an extension that constrains type parameter representations?

Yes. When checking the definition of (type 'a t = ...), it must not contain conflict for any possible value of ('a). On the other hand, when computing the head shape of (int t), we do use our knowledge of the parameter (int) to compute a precise shape -- the shape is computed on the definition of (t) with ('a) known to be int.

> Line 85: this might be a good place to introduce OCaml's tagging system

not sure about this suggestion, I like to keep the introduction as lightweight as possible

> Line 100: a diagram of head shapes might be useful, e.g. showing how heads and headshape multisets relate
> 
> Line 110: isn't it true that a definition of heads depends on both the language and its implementation (i.e. a specific target)?

The designer of the head shape system can choose either very precise head shapes, that are specific to one implementation of the language, or more coarse head shapes, that reject more unboxing annotation but are portable to more backends. In the case of OCaml, our current preference would go for defining a head shape that is a least common denominator between the native/bytecode, Javascript and wasm backends. (There is a tension between being able to guarantee more performance for users that know that they want to specialize on a given backend, and providing portable performance guarantees across implementations. One could think of interface hints for this, for example by distinguishing [@unboxed must] and [@unboxed if_possible].)

> Line 166: it probably makes sense to also declare OCaml's universal data representation which it uses to implement polymorphism?

I am not sure what this question means. Maybe the suggestion is to point out that the representation we describe is uniform (not type-dependent), and thus can be used to implement polymorphism without specialization.

> Line 235: perhaps the way to say this is that it made more code safe by removing essentially a user-encoded tag switch and replacing it with a compiler-generated, safe one?

My view is that the previous code used (correctly) features that, if misused, break type and memory soundness, while the new code uses features that may fail statically but always preserve type and memory soundness. (Caveat: any use of the FFI, and the use of the shape assumption on an abstract type, break this safety property.)

> Line 249: this is provided that the representations available are powerful enough to allow unboxing

The wording is explicit I think ("In some cases, ...") that our simple form of constructor unboxing can only replace a strict subset of the unsafe-code usage scenarios.

> Line 257: K is never explained properly. This must be the universe of tags?

clarified through a "Notations" paragraph right before 3.1.

> Line 286: consider rewording this for readers that don't immediately recall what injective means

Good remark! Done.

> Line 293: This sentence is kind of circular; I think you want to say that this definition captures the fact that unboxed constructors are represented the same as their value.

removed

> Line 312: What is K here again?
>
> Line 329: this implies that we cannot have data representations with unused bits

Not quite, I think:
- Our type Data is not necessarily a bit-level description of the Data,
  it can typically be higher-level and not specify full bit placement.
  It is then up to the implementation to map data values to bit layouts, and this mapping could introduce unused bits.
- Even if Data was a bit-level layout, Heads are an abstraction
  of the data that ignore some information, so they can forget
  about irrelevant bits.

> Line 335: this would be helpful as a bulleted list, considering it's important syntax

Yes, but space is scarce.

> Line 339: do these two macros even need to exist? they could be inlined where used with probably no less readability

I like to forget that things are defined as functions and manipulate multisets as abstract types.

> Line 354: this formulation seems very tied to the polymorphic implementation strategy with a universal representation. Does this generalize?

Our intuition is that the formulation (taking the maximum of all closed instances) works for non-regular type representations as well. Consider for example a non-uniform variant of OCaml where `char array` is represented as a `string`, and `bool array` as a bitset. The head shape of the open type expression `'a array` is then any usual array, or a string, or a bitset. This does not depend on whether arrays, strings and bitsets are explicitly tagged in their runtime representation to be distinguishable from each other.

Of course, the reduction of dynamic type information in such non-uniform implementations can reduce unboxing opportunities, as `type 'a t = String of string [@unboxed] | Arr of 'a array [@unboxed]` is not accepted anymore.

TODO consider whether we want to change the paper to accomodate this question of the review.

> Line 384: top not explained yet
> 
> Line 395: is this overload of the matching function necessary?

TODO use [|_|]_Z for the right-hand-side function in P(Z).

> Line 437: I think this is also assuming a universal representation?

Making such an assumption was not our intention. On re-reading the whole section carefully, we made the mistake of defining constructor declarations as type components (line 345) as
  C of (tau1, tau2...)
without including their *return* type. This presentation implicitly assumes that a given constructor name will always have the same representation. Instead we should extend our `repr : Value -> Data` function to take type information `repr: Value -> Type -> Data`, and define our components with an explicit return type
  C : (tau1, tau2...) -> (tau1', ...) t

(Note that as long as the datatype declaration in our model remain in `C of (tau1, tau2...)` syntax, then this change does not introduce the full expressivity of GADTs, the closed type components obtained in this way will be instantiations of the non-generalized scheme
  C : (tau1...) -> (alpha1...) t
)

With this small fix, we hope that it is clear that we are not making universal representation assumptions. For example, our model of datatype declarations supports adding an `option` type where different instances `foo option` use different representations for the `None` and `Some` constructors -- then the head shape of `'a option` will be a union (maximum) of those possible representations.

Of course, our analysis (and our formal treatment of types with free type parameters) is meant to answer the question of "is `'a option` confusion-free *for any instance of `'a`", and this may not be the right question to ask for some languages. If the language uses agressive specialization of all polymorphic parameters (there is a synergy with non-uniform representations), it may be content with checking the absence of confusion for all monomorphic instances used in the program.

DONE we use (C of (tau_i)^i : tau) to explicitly indicate the return type of type components.

> Line 496: can all of these now be expressed as unboxing (perhaps with your extensions?)

For `lazy`, as we discussed previous the answer is "not really" (or "you need even more extensions that we are not enthusiastic about"). For arrays, we could indeed consider having a data-structure defined in OCaml as

```ocaml
type 'a array =
| Addr of 'a generic_array [@unboxed]
| Float : floatarray -> floatarray [@unboxed]
```

(`generic_array` would be an array tag that always has tag 0 and never tries to unbox floats. It not currently exist in the OCaml standard library (expect when enabling the non-standard `-no-flat-float-array` mode, then `'a array` behaves like this), although some users such as Coq have asked for it. `floatarray` is an existing name for the type of flat float arrays, available in the stdlib of recent-enough OCaml versions through the submodule `Float.Array`.)

This would be enough to define several array-accessing functions in pure OCaml, but for array creation the runtime uses a dynamic test to say if its argument is a float; implementing it in OCaml would require exposing an extra (inelegant but safe) primitive to OCaml users:

```
val guess_if_float : 'a -> ('a, float) Type.eq option
```

DONE discussd in the paper

> Line 542: the intersection of value representations in these might not be that big
> 
> Line 557: this is actually a GREAT CONTRIBUTION and could be pulled up into intro
> 
> Line 569. Yeah.
> 
> Line 605: why not an explicit ```[@boxed]``` annotation to break recursion?
> 
> Line 667: does this explict need polymorphism?

(I don't understand the question.)

> Line 674: this is basically manually pushing value representations apart

yes, exactly

> Line 690: this could potentially make an ADT non-referentially transparent if the boxing function is not pure

I clarified in the article that `(add 1)` here is not meant as an arbitrary OCaml function, but an expression in a fixed transformation DSL. In particular, pattern-matching needs to be able to invert it.

> Line 717: but presumably these would be cached by default?

If the result of the transformation appears in the OCaml source as
a constant, then it can be preallocated in the data segment, providing
a form of caching at little implemeentation complexity. This would
apply for example if the generated code contained something akin to:

```ocaml
(function
  (* transformation function
    from constant to non-constant constructor *)
  | A -> A' ()
  | B -> B' ()
  | ...)
```

or

```ocaml
(function
  (* transformation function
    from constant to non-constant constructor *)
  | A -> K 0
  | B -> K 1
  | ...)
```

in either forms all right-hand-sides get preallocated.

TODO: consider whether we want to write something about this in the paper. I (Gabriel) don't think we want to afford the space beyond the current parenthetical remark. (Let us spend more time on the things
that we have actually done, rather than the things we might do in
the future.)

> Line 1152: Please also compare to EverParse by Nikhil Swamy and MSR.

TODO: on first skim, EverParse is a language for parsing (with dependently-typed specifications), but it does not clearly help for data layout ().
