#!/bin/bash
MAIN=unboxing-extended
BIB=unboxing

mkdir arxiv || { echo "error, you need to (rmdir arxiv)"; exit 1; }
rm -f arxiv.zip

cp $MAIN.tex $MAIN.cfg arxiv/

cp $BIB.bib arxiv/ # (unused) source .bib, for reference
stat $MAIN.bbl > /dev/null || { echo "you need to run bibtex first"; exit 1; }
cp $MAIN.bbl arxiv/

cp *.sty arxiv/

cp acmart.cls arxiv/

(
    echo "pdflatex $MAIN.tex"
    echo "pdflatex $MAIN.tex"
    echo "pdflatex $MAIN.tex"
) > arxiv/build.sh

zip -r arxiv arxiv
echo "feel free to test the packed source in arxiv/, archive is arxiv.zip"
