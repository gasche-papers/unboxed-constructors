set -x
git filter-repo --force --refs $(cat ../censoring-git-repo/refs) --mailmap ../censoring-git-repo/mailmap-anon --path Changes --invert-paths --replace-text ../censoring-git-repo/to-censor.txt --message-callback 'return re.sub(b"nchataing", b"anonymous", message)'
