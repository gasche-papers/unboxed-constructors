set -x
rm -fR /tmp/ocaml-with-unboxing-git-repo
git clone file://$(pwd) --depth 58 /tmp/ocaml-with-unboxing-git-repo
(cd /tmp ; zip -r ocaml-with-unboxing-git-repo.zip ocaml-with-unboxing-git-repo)
du -sch /tmp/ocaml-with-unboxing-git-repo.zip
