We thank the reviewers for their detailed comments and thougful
feedback. It appears that there is no hope for this paper as
submitted, and we have to go back to the writing board to present our
work better. In case the reviewers are curious, we did include
comments to the salient points of their reviews below.


# Review A

Thanks for the detailed feedback and writing advice.

> There are a numerous points to be made about the contributions' list
> but the most striking one is that the main motivation behind the
> paper (its even on the title) is presented as merely "an unplanned
> side-effect" of the whole activity.

We are not sure what is the problem with stating clearly that
a particular scientific result is an "unplanned side-effect" of adding
an apparently-unrelated feature to a programming language.

To be clear:

- We believe that the contribution on termination-checking is
  interesting, and could possibly be reused by other people. We
  invested considerable in Section 5 to ensure this.
  
- Pointing out the relation to cpp is just for fun, not a central
  contribution of the paper, and we mention it in the title because we
  found it amusing.

We are not sure which of the two aspects the authors has in mind as
being problematically described as an "unplanned side-effect".


> - The 3rd tree expansion example in Fig.1 requires some than
>  a single expansion step, doesn't it?

No, a single expansion step suffices. This is a head expansion for the
subtree starting at node `c`, `c(d,e(f))`, which expands into
`c'(c"(X,Y),X)` where `X` is `e(f)` and `Y` is `f`.

# Review B

> A good chunk of the paper is dedicated to the formalization of the
> normalization result. In itself this is described as a known
> result. However I do understand that the general result may not be
> practical for the current application, where only normalization of
> annotated constructors is required.

Different proof arguments for deciding termination give different
algorithms for checking termination for a given term. Existing
termination-decision proofs give algorithms that are not "zero cost",
in the sense that work is required on parts of the program's type
definitions that do not contain unboxing annotations and are not
mentioned from those. We propose a new decision procedure that is
zero-cost in this sense.

> It would be nice to see some experimental data indicating that this
> is indeed practical. The submission describes the problems of
> extending the basic idea into an implementation for full OCaml, but
> stops there. In principle the described approach could be horribly
> slow in terms of analysis/normalization time: It is hard to tell
> from the current submission!

We have a prototype implementation where we observed that
unboxed-checking add no observable overhead in practice. We also
*knew* that it would be the case -- being familiar with the OCaml
compiler and its compile-time performance characteristics. You are
making an excellent point that we should convey this intuition better
in the paper.

> Suggestions for experiments:
> - Without any [@unboxed] annotations, does the OCaml compiler run
>   without noticable slowdowns? Also for a self-compile?

There is no cost in absence of [@unboxed] annotations, by design: the
code we added only runs on constructors with the annotation, once per
constructor.

> - How much (compile) time did the safer variant of Zarith with
>   [@unboxed] annotations take compared to the unsafe original?

There is no measurable compile-time difference with the unboxed
annotations. The [@unboxed] check is immediate, dwarfed by the rest of
the compilation process.

> - One can of course also imagine experiments on other/larger OCaml
>   code bases, with and without added annotations.

It may be illustrative to perform some back-of-the-enveloppe
complexity estimation, paired with assumptions about user
programs.

The computation time for shapes depends mostly on the length L of the
unfolding sequence; we reduce annotations or unboxed constructors in
head position in the type expressions, until we reach a type
constructor that is not an abbreviation and does not contain unboxed
constructors. The worst-case length of L is bounded (with our
termination-monitoring strategy) by the number of type definitions in
the program, it could be tens of thousands for very large programs;
but in practice L will be very small, typically 1, 2 in the vast
majority of cases and maybe 10 or 20 for some abstraction-heavy
programs. (Even erroneous human-written programs whose unfolding would
loop perform a small number of unfolding steps before they reach
a cycle.)

Then each reduction step maintains a map annotating each
sub-type-expression with a trace, and the work is bounded by the size
S of the type expression. Some OCaml programs using structural types
can have type expressions of thousands of nodes.

All told, the shape-computation work is bounded by the product S*L,
which could be of order 10^4 for programs using deeply nested chains of
type definitions expanding to very large types. The computation for
these cases should be instantaneous on modern machines, and add no
measurable overhead to the total compile time.

If the programming style evolves over time, unboxed constructors
become heavily used (in particular if we moved from an opt-in to an
enabled-by-default interface) the computation time might start being
noticeable, and in this case we could consider further optimizations,
such as trying to cache normalized information in some intermediate
abbreviations. Clearly the current usage modes do not justify such an
extra effort and extra complexity.

> In Sec.5.3 a term's annotations are described as lists without
> duplicates. I'm wondering if annotating with a set would be slightly
> simpler? The later ordering already uses super-set ordering among
> sets.

Conceptually both are fine (the usual subset of BNF notation used in
our community makes lists easier to describe than sets). Our actual
implementation uses lists, and it is useful to preserve the ordering
for error messages showing the cycle to the user.

> The related work section is a bit thin. I was reminded of a few older
> papers on data representation optimizations:
> - Steenkiste's "The Implementation of Tags and Run-Time Type Checking"
>   from "Topics in Advanced Language Implementation" 1991 and
> - Henglein's "Global Tagging Optimization by Type Inference" from LFP1992.
> None of these are however on constructor unboxing, but investigates
> ways to optimize tagging and untagging operations.

Thanks! (I was not able to find an online copy of the first work
cited.) In the OCaml community, Henglein's work would remind me of
Leroy's type-based analyses for float unboxing also done in the early
90's. In general it was found that those approaches did not pay for
their complexity compared to local optimizations (plus inlining). The
present work is aimed at letting advanced programmers explicitly
request tag elisions (safely ) rather than automated inference.


# Review C

> - Section 6 on the C preprocessors is insufficient. It is not enough
>   to "believe" that the C preprocessor (cpp) fits a particular
>   element of the soundness proof of Section 5 to declare that
>   Section 5 is a full proof of its termination. The authors should
>   show this connection much more formally, beyond just saying "we
>   believe." More globally, this paper leaves the impression that the
>   work of Section 5 is half-baked: Section 1.5 suggests that the
>   proof in Section 5 "may" give rise to an implementation, and the
>   C preprocessor may very well be such an implementation, but
>   Section 6 does not substantiate that claim.
>
> * At the very least, I would have expected a formal algorithm
>   describing the C preprocessor as an instantiation of the formalism
>   of Section 5; on top of that, even just a proof-of-concept
>   implementation would be welcome, to compare its results to some
>   public cpp test suite (say, a subset of
>   https://gcc.gnu.org/onlinedocs/gccint/C-Tests.html ) to check that
>   the algorithm follows the C standard as described in Section 6 –
>   and thus, that cpp indeed fits the formalism of Section 5. With
>   the shortcomings of Section 6 as it is now, the title of this
>   paper sounds like clickbait.
>
> * In fact, without implementation, I am not sure of the added value
>   of Section 5 compared to previous proofs of the decidability of
>   termination of order-1 recursive program schemes.  There is
>   a misunderstanding here.

We have an implementation of the proposed OCaml feature, including the
termination-monitoring algorithm, in our experimental fork of the
OCaml compiler:
  https://github.com/gasche/ocaml/tree/head_shape
(We mentioned this in the Contributions paragraph. We apparently
failed to provide an explicit URL; note that it is reachable from
Jeremy Yallop's RFC discussion which is itself explicitly linked in
the paper.)

When we say that our termination-checking approach may be more
amenable to an implementation, our OCaml prototype is the
implementation that we have in mind -- not cpp implementations. We are
not aware of other implementations of decision procedures for
first-order or higher-order recursion schemes being used in
practice. We believe that the standard proof approach (and the
decision procedure it suggests), by global interpretation of all
definitions, would not be a good fit (too complex and possibly slow)
for the OCaml compiler -- we would have to run a global computation on
all type definitions, and to do this several times at various points
during type-checking.

The cpp specification is fairly algorithmic in wording (it reads like
an attempt to describe a reference implementation in obscure prose),
so it is plausible that existing implementions contain a similar
algorithm. A precise implempentation was proposed in 1986 by Dave
Prosser, see https://www.spinellis.gr/blog/20060626/ ; but it seems
that current cpp implementations use simpler, incomplete algorithms,
see https://stackoverflow.com/a/67854353/298143 .

We agree that section 6 could have more details. But we should also be
realistic that this is more of a curiosity: C implementations try to
support a larger feature set with higher-order macros etc., and they
do not care about completeness. Our work is extremely unlikely to
influence their code in any way. So yes, we can make the connection
more detailed, but this is just for fun. It is unfortunate that,
probably due to a miscommunication on our part, this aspect was so
central in the review's evaluation of our work.

> - The related work and references are quite light. Dargent is now
>   published (POPL 2023: https://doi.org/10.1145/3571240 ) and its
>   related work section should be a valuable source of inspiration
>   for this submission here. Even from a higher-level point of view,
>   there should be a more thorough discussion to compare the authors'
>   approach with specialization approaches mentioned in Section 7,
>   both in terms of expressiveness and in terms of compile-time and
>   runtime performance.

Thanks! These are good, actionable recommandations.

Our Related Work section is thin because we found it difficult to
meaningfully relate this work to previous works:

- We are not aware of a programming language offering constructor
  unboxing outside the obvious case of variant types with a single
  constructor. Most programming languages with automatic unboxing
  (we discuss Rust and MLton in our Related Work) use
  specialization-based approaches that are unsuitable for languages
  implementations such as OCaml that support separate compilation.
  
- Most of the work on "specialized data-layout languages" has focused
  on record layout or bit formats, which are not clearly relevant to
  our work. (There is typically no issue with ensuring that
  disjointness is preserved.)
  
  Dargent does handle algebraic datatypes / variant types, but it
  assumes a fixed bit range for all constructors of the variant, so
  disjointness is enforced by construction without any need for
  a detailed analysis or even a detailed specification -- in contrast,
  they detail lens-like equational proof obligations for the getters
  and setters of their record type. We looked at Dargent hoping to
  find a formalization of this idea of disjointness, but it is not
  really there for now. One could certainly think of a future
  extension of Dargent supporting constructor unboxing, possibly
  inspired by our work.

