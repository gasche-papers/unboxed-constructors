\input{\jobname.cfg}
\newcommand{\Beamer}{\True}
\newcommand{\acmart}{\False}

\documentclass[usenames,dvipsnames,table]{beamer}
\usepackage[utf8]{inputenc}

\usepackage{reversion}
\usepackage{mylistings-slide}
\usepackage{mybiblio}
\usepackage{mymath}
\usepackage{myhyperref}
\usepackage{notations}
\usepackage{mybeamer}

\hypersetup{breaklinks=true,colorlinks=true,citecolor=purple}

\usetheme{Boadilla}
\setbeamertemplate{navigation symbols}{}
\setbeamertemplate{footline}{\hfill\insertframenumber\hfill\vspace{3mm}}

\title{Unfolding ML datatype declarations without loops}
\date{\today}
\author{Nicolas Chataing, Stephen Dolan, Gabriel Scherer} % ordre alphabétique
%\institute{Partout, Inria Saclay \& LIX, France}

\begin{document}

\begin{frame}
  \titlepage

\begin{center}
  \includegraphics[height=3cm]{pictures/Nicolas_Chataing.jpg}
  \hspace{2em}
  \includegraphics[height=3cm]{pictures/Stephen_Dolan.jpg}
  \hspace{2em}
  \includegraphics[height=3cm]{pictures/Gabriel_Scherer.jpg}
\end{center}
\end{frame}

\begin{frame}{This talk}
  An OCaml feature we wanted: constructor unboxing.

  \vfill

  A general (language-agnostic) problem we solved:\\
  $\quad$ unfolding of (recursive) type declarations, in a terminating way.

  \vfill

  Full paper (JFLA 2022):\\
  \url{http://gallium.inria.fr/~scherer/research/constructor-unboxing/constructor-unboxing-jfla.pdf}

  (upcoming POPL'24 publication)
\end{frame}

\begin{version}{\Long}
\section{Constructor unboxing}
\frame{\sectionpage}
\end{version}

\begin{frame}[fragile]{Coproducts}
\begin{mathpar}
(A + B)

\text{often implemented as}

(\{0\} \times A) \cup (\{1\} \times B)
\end{mathpar}

\vfill

But sometimes $A \cup B$ is enough, or $(\{0\} \times A) \cup B$\dots

\vfill

In OCaml:
\begin{lstlisting}
  type 'a btree =
  | Leaf of 'a
  | Node of 'a btree * 'a * 'a btree
\end{lstlisting}
\end{frame}

\begin{frame}[fragile]{Constructor unboxing}
  Single-constructor unboxing: in OCaml since November 2016
\begin{lstlisting}
type id = Id of int [@@unboxed]
\end{lstlisting}

\hfill

\pause
Extension proposed by Jeremy Yallop in March 2020:
\begin{center}
\href{https://github.com/ocaml/RFCs/pull/14}{OCaml RFC \#14: constructor unboxing}

\includegraphics[height=3cm]{pictures/Jeremy_Yallop.jpg}
\end{center}
\pause
\begin{lstlisting}
type bignum =
| Small of int [@unboxed]
| Big of Gmp.t
\end{lstlisting}

\hfill(\texttt{int} and \texttt{(Big of Gmp.t)}: disjoint representations)
\end{frame}

\begin{version}{\Long}
\begin{frame}[fragile]{OCaml value representation}
  The compiled representation of an OCaml value is a machine word, either:
  \begin{itemize}
  \item an \emph{immediate} (lsb = 1):
    \\$\quad$ integers, booleans, constant constructors
  \item a \emph{block} (lsb = 0), the address of a memory area containing\\
    a \emph{header} (including a \emph{tag}) and zero, one or several OCaml values:
    \\$\quad$ floats, strings, non-constant constructors, tuples, arrays...
  \end{itemize}

  \vfill\pause
  Morally:
\begin{lstlisting}
type value =
| Imm of int
| Block of { tag : int; args: value list }
\end{lstlisting}

  \vfill

  Example:
\begin{lstlisting}[columns=flexible]
type t =
| True           (* Imm 0 *)
| False          (* Imm 1 *)
| Not of t       (* Block { tag = 0; args = [v] } *)
| And of t * t   (* Block { tag = 1; args = [v1; v2] } *)
\end{lstlisting}

(variants: GHC always uses blocks, low constructors stored in the pointer.)
\end{frame}
\end{version}

\begin{frame}{Head, head shape}
  We define the \emph{head} of an OCaml value, in $\{\mathtt{Imm},\mathtt{Block}\} \times \mathbb{Z}$, by:
  \begin{itemize}
  \item the head of an immediate is the immediate itself \\
    \texttt{head(42) = (Imm, 42)}
  \item the head of a block is its tag \\
    \texttt{head("foo") = (Block, Obj.string\_tag) = (Block, 252)}
  \end{itemize}

  \vfill

  We define the \emph{head shape} of a type as set of heads of its values:
  \[ \mathtt{head}(\tau) = \{ \mathtt{head}(v) \mid v : \tau \} \]
\end{frame}

\begin{frame}[fragile]{Unboxing specification}
\begin{lstlisting}[columns=flexible]
type bignum =                          match num with
| Small of int        (* Block 0 *)    | Small n -> ...
| Big of Gmp.t        (* Block 1 *)    | Big gmp -> ...
\end{lstlisting}

\vfill

Unboxing constructors is valid if the head shapes remain disjoint.

\vfill

\begin{lstlisting}[columns=flexible]
type bignum =                                 match num with
| Small of int [@unboxed]   (* Imm $\mathbb{Z}$ *)        | Small n -> ...
| Big of Gmp.t               (* Block 0 *)    | Big gmp -> ...
\end{lstlisting}

\vfill

(20\% performance gain)

\vfill

Constructors: runtime-checkable disjointness.\\

(Note: This morality is language-independent.)
\end{frame}

\begin{version}{\Long}
\section{Extensions (future work)}
\frame{\sectionpage}

\begin{frame}[fragile]{Trusted shapes for FFI types}
Some abstract types are populated by the FFI,
one can assert their shape.

\vfill

\begin{lstlisting}
module Gmp = struct
  type t [@shape [custom]]
  ...
end

type bignum =
| Small of int [@unboxed]
| Big of Gmp.t [@unboxed]
\end{lstlisting}

\vfill

(Prototype implementation)
\end{frame}

\begin{frame}[fragile]{Arity}
  Tracking the arity in shapes would separate more types.

  \begin{lstlisting}[columns=flexible]
    type 'a pair = 'a * 'a           (* Block 0, arity 2 *)
    type 'a triple = 'a * 'a * 'a    (* Block 0, arity 3 *)

    type 'a foo =
      | Pair of 'a pair [@unboxed]
      | Triple of 'a triple [@unboxed]
  \end{lstlisting}

  \vfill

  Future work!
\end{frame}

\begin{frame}[fragile]{Non-default representations}
  \begin{lstlisting}
    type rigidity = Rigid | Flexible

    type status =
      | Active of rigidity [@unboxed]
      | Generic [@tag 2]
  \end{lstlisting}

  \vfill

  Note: no change-of-representation at (de)construction.

  \vfill

  Future work!
\end{frame}
\end{version}

\begin{version}{\Long}
\section{Computing head shapes}
\frame{\sectionpage}
\end{version}

\begin{frame}{Computing the head shape?}
  How to compute the head shape of a type?

  ~\\

  (In presence of recursive type declarations)
\end{frame}

\begin{frame}[fragile]{Computing the head shape?}
\begin{lstlisting}
type 'a tree = Node of ('a * 'a tree) seq [@unboxed]
and 'a seq = Nil | Next of (unit -> 'a * 'a seq) [@unboxed]
type foo = Foo of int tree [@unboxed] | ...
\end{lstlisting}

\vfill

\begin{lstlisting}
  shape(int tree)
= shape((int * int tree) seq)
= shape(Nil) + shape(... -> ...)
= Imm 0 + function_shape
\end{lstlisting}

\vfill\pause

Expanding a type definition is a $\beta$-reduction.

Call-by-name normal form... with arbitrary recursion.

\vfill\pause

\begin{lstlisting}
let rec
  tree a = seq (prod a (tree a))
  seq a = nil + (arrow unit (prod a (seq a)))
  foo = tree int + ...
in tree int
\end{lstlisting}
\end{frame}

\begin{frame}[fragile]
cycles:

\begin{lstlisting}
type t = U of u [@unboxed] | Bar
and u = T of t [@unboxed]

let rec
  t = u + bar
  u = t
in t
\end{lstlisting}

\vfill

How to prevent nontermination?

\vfill

This is useful for many static analyses of types:\\
head shape, immediacy, etc.
\end{frame}

\begin{frame}[fragile]{Attempt 1: rule out cycles statically}
  ``Statically'': without expanding definitions.

  \vfill

  (As done for type synonym/aliases.)

  \vfill

  Problem: too restrictive

  \vfill

  \begin{lstlisting}
    type 'a seq = ...

    type 'a tree = Node of ('a * 'a tree) seq [@unboxed]
  \end{lstlisting}
\end{frame}

\begin{frame}[fragile]{Attempt 2: prevent repetition of whole types}
  Keep track of type inputs, abort if they come again during expansion.

  \vfill

  Problem: may loop in presence of non-regular type parameters.

  \vfill

  \begin{lstlisting}
    type 'a bad = Loop of ('a * 'a) bad [@unboxed]
  \end{lstlisting}

  \vfill

  \begin{lstlisting}
    let rec
      bad a = bad (prod a a)
    in
    $\quad$ bad int
    $\rightarrow$ bad (prod int int)
    $\rightarrow$ bad (prod (prod int int) (prod int int))
    $\rightarrow$ ...
  \end{lstlisting}
\end{frame}

\begin{frame}[fragile]{Attempt 3: prevent repetition of head constructors}

  Keep track of constructors (functions) that have already been expanded.\\

  Abort if an expanded constructor comes again in head position.

  \vfill

  Problem: too restrictive

  \vfill

  \begin{lstlisting}
    type 'a id = Id of 'a [@unboxed]
    type foo = Foo of int id id [@unboxed]
  \end{lstlisting}

  \vfill

  \begin{lstlisting}[columns=flexible]
    let rec
      id a = a
      foo = id (id int)
    in
    $\quad      $ foo                         []
    $\rightarrow$ id (id int)                 [foo]
    $\rightarrow$ id int                      [foo, id]
    $\not\rightarrow$
  \end{lstlisting}
\end{frame}

\begin{frame}[fragile]{Solution: annotate (sub)expressions with expansion context}
  \begin{lstlisting}
    type 'a id = Id of 'a [@unboxed]
    type 'a delay = Delay of 'a id [@unboxed]
    type foo = Foo of int delay delay [@unboxed]
  \end{lstlisting}

  \vfill

  \begin{lstlisting}[columns=flexible]
    let rec
      id a = a
      delay a = id a
      foo = delay (delay int)
    in
    $\quad      $ []foo
    $\rightarrow$ [foo]delay ([foo]delay [foo]int)
    $\rightarrow$ [foo,delay]id ([foo]delay [foo]int)
    $\rightarrow$ [foo]delay [foo]int
    $\rightarrow$ [foo,delay]id [foo]int
    $\rightarrow$ [foo]int
  \end{lstlisting}

  Track when subexpressions \emph{appeared} in the type,\\
  not how they came to head position.

  \vfill\pause

  (Remark: similar to \texttt{cpp} termination control.)
\end{frame}

\begin{frame}[fragile]{Termination proof}
  Suprisingly tricky!

  \href{https://github.com/ocaml/ocaml/pull/10479#issuecomment-876644067}{https://github.com/ocaml/ocaml/pull/10479\#issuecomment-876644067}

  With help from Irène Waldspurger.

  \vfill

\begin{center}
  \includegraphics[height=3cm]{pictures/Irene_Waldspurger.jpg}
\end{center}

  \vfill

  Usual decreasing-measure argument, with multisets of multisets.
\end{frame}

\begin{frame}{Completeness ?}
  Our algorithm is complete for the pure
  \emph{first-order} $\lambda$-calculus with recursion.

  ~\\

  (OCaml/SML type definitions: in \code{type 'a t = ...}, \code{'a} has kind $\star$.)

  \vfill\pause

  A \emph{decision} procedure for the halting problem!
\end{frame}

\section{Decision procedure(s)}

\newcommand{\height}[1]{\mathsf{height}\,#1}

\begin{frame}{Surprisingly... unsurprising}
  Known result: \\ $\quad$
    termination is \emph{decidable}
    in the simply-typed $\lambda$-calculus with recursion.

  (See for example \citet*{higher-order-halting-problem})

  \vfill

  Intuition: types interpreted as finite lattices
  \begin{mathpar}
    \tau
    \bnfequal
    o \mid \tau \to \tau \mid \tau \times \tau
    \\
    \begin{array}{rll}
      \sem o & \defequal & \{ \bot \leq \top \}
      \\
      \sem {\tau_1 \times \tau_2} & \defequal & \sem{\tau_1} \times \sem{\tau_2}
      \\
      \sem {\tau_1 \to \tau_2} & \defequal & \mathsf{MonFun}(\sem{\tau_1}, \sem{\tau_2})
      \\
      \\
      \sem {\mathsf{Y}_\tau} & \defequal & \lam f {f^{(\height{\sem \tau})}}
    \end{array}
  \end{mathpar}
\end{frame}

\begin{frame}{Simply-typed lattices}
  $\sem{((o \to o) \to o)\dots \to o}$ ($n$ occurrences of $o$) has a very \emph{simple} structure.

  It is the linear order $[0; n]$.

  \vfill

  $\sem{o \to o \to \dots \to o}$ ($n$ occurrences of $o$) has a very
  \emph{complex} structure.

  Its cardinality (2, 3, 6, 20, 168\dots) is the ``Dedekind Problem''.

  (Height is $2^{n - 1} + 1$.)

  \vfill

  Note: we could restrict to \emph{stable} monotonous functions.
\end{frame}

\begin{frame}[fragile]{Comparing algorithms}
\begin{lstlisting}
let rec
  f a = ...
  g a = ...
  h a b c = ...
in ...
\end{lstlisting}
A fixpoint at type $(o \to o) \times (o \to o) \times (o \to o \to o \to o)$.

\begin{mathpar}
  \height {(\tau_1 \times \tau_2)} = \height{\tau_1} + \height{\tau_2}
\end{mathpar}

Lattice height: expansion depth bounded by $2 + 2 + 9 \quad (- 1 )$.

~\\

Our algorithm: expand each of \code{f}, \code{g}, \code{h} at most once.

~\\

(Even at $o \to o$: 2 expansions vs. 1.)

~\\

Our first-order algorithm seems much better.

Do better higher-order algorithms also exist?
\end{frame}

\begin{frame}{Summary}
  Unboxed constructors: an optimization requiring type analysis.

  \vfill

  Normalizing types in presence of cyclic references.

  \vfill

  Curious questions on STLC+Y.

  \vfill

  \begin{center}
    Thanks! Questions?
  \end{center}
\end{frame}

\begin{frame}
\bibliography{long/unboxing}
\end{frame}
\end{document}
