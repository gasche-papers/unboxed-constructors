# Unboxed constructors -- how, or cpp solves a halting problem

## Practice and theory

Practice: a low-level data representation feature for OCaml

Theory: unfolding type definitions without looping

# Practice

## Constructor unboxing

```
type id = Id of int

Cons(Id 42, Nil)
```

Single-constructor unboxing:
```
type id = Id of int [@@unboxed]

Cons(Id 42, Nil)
```

(manual implementation because it affects the FFI)
(low performance impact in the common case)

Constructor unboxing:
```
type bignum =
  | Small of int [@unboxed]
  | Large of Gmp.t [@unboxed]
```

Perf: 30% performance gain on a `bignum` micro-benchmark.
Potential locality gains on space-hungry programs.


## Forbidding confusion

```
type t =
  | Id of int [@unboxed]
  | Error of error_code [@unboxed]
and error_code = int
(*
Error: This declaration is invalid, some [@unboxed] annotations introduce
       overlapping representations.
*)
```

A static analysis at type-declaration time:
- abstract/approximate (closed) types into *shapes*,
- compute the shape of type expressions and variant constructors,
- fail if constructor unboxing introduces non-disjoint shapes 

Shapes for the OCaml value representation:
- for an immediate value $n \in Z$: ${Imm, n}$
- for a block of tag $t$: ${Block, t}$.

Remark: no specialization/monomorphisation in OCaml, but we could of
course unbox more in that context.

# Theory

## Shape computation: beta-reduction

```
type 'a tree = Node of ('a * 'a tree) seq [@unboxed]
and 'a seq = Nil | Next of (unit -> 'a * 'a seq) [@unboxed]
type foo = Foo of int tree [@unboxed] | ...

shape(int tree)
= shape((int * int tree) seq)
= shape(Nil) + shape(unit -> (int * int) seq)
= {Imm, 0} + {Block, function_tag}
```

Beta-reduction,
in the first-order lambda-calculus (with only functions),
with `let rec`.

```
let rec
  tree a = seq (prod a (tree a))
  seq a = Nil + arrow unit (prod a (seq a))
in tree int
```

```
type loop = Loop of loop [@unboxed]

let rec
  loop = loop
in loop
```

Deciding termination?

(Show of hands)

## Approach 1

statically forbid cycles: reject

```
type 'a tree = Node of ('a * 'a tree) seq [@unboxed]
```

too restrictive


## Approach 2

Block reduction when the same type expressions occurs again.

Not enough.

```
let rec
  loop a = loop (prod a a)
in loop int

  loop int
= loop (prod int int)
= loop (prod (prod int int) (prod int int))
= ...
```


## Approach 3

Block reduction when the same type *constructor* occurs again.

Too restrictive:
```
let rec
  id a = a
in id (id int)

  []   id (id int)
= [id] id int
(* blocked *)
```

## Solution

Track head constructors in all sub-expressions.

```
let rec
  id a = a
  delay a = id a
  foo = delay (delay int)
in foo

[]foo
= [foo]delay ([foo]delay [foo]int)
= [foo,delay]id ([foo]delay [foo]int)
= [foo]delay [foo]int
= [foo,delay]id [foo]int
= [foo]int

Sound and complete for first-order systems, incomplete above.

## cpp

A termination-tracking algorithm by Dave Prosser from the 80s.

## Types-list to the rescue

Recursion does not always help

unexplained gap in the first-order case

further work in the HOMC community, in particular by Kobayashi
