\input{\jobname.cfg}
\newcommand{\Beamer}{\True}
\newcommand{\acmart}{\False}

\documentclass[usenames,dvipsnames,table]{beamer}
\usepackage[utf8]{inputenc}

\usepackage{reversion}
\usepackage{mylistings-slide}
\usepackage{mybiblio}
\usepackage{mymath}
\usepackage{myhyperref}
\usepackage{notations}
\usepackage{mybeamer}

\hypersetup{breaklinks=true,colorlinks=true,citecolor=purple}

\usetheme{Boadilla}
\setbeamertemplate{navigation symbols}{}
\setbeamertemplate{footline}{\hfill\insertframenumber\hfill\vspace{3mm}}

\title{Constructor unboxing \\ -- or, how \text{cpp} decides a halting problem}
\date{\today}
\author{Nicolas Chataing, Stephen Dolan, Gabriel Scherer, Jeremy Yallop} % ordre alphabétique
%\institute{Partout, Inria Saclay \& LIX, France}

\begin{document}

\begin{frame}
  \titlepage

\vfill

\begin{center}
Low-level data representation feature for OCaml.

Theory surprise.
\end{center}

\vfill

\begin{center}
  \includegraphics[height=3cm]{pictures/Nicolas_Chataing.jpg}
  \hspace{1em}
  \includegraphics[height=3cm]{pictures/Stephen_Dolan.jpg}
  \hspace{1em}
  \includegraphics[height=3cm]{pictures/Gabriel_Scherer.jpg}
  \hspace{1em}
  \includegraphics[height=3cm]{pictures/Jeremy_Yallop.jpg}
\end{center}
\end{frame}

\begin{version}{\Long}
\section{Practice}
\frame{\sectionpage}
\end{version}

\begin{frame}[fragile]{Single-constructor unboxing}
\lstset{
  keepspaces=true,
}
\begin{lstlisting}
type id = Id of int

            Cons(Id 42, Nil)
\end{lstlisting}

\vfill\pause

Single-constructor unboxing (since 2016):
\begin{lstlisting}
type id = Id of int [@@unboxed]

source:     Cons(Id 42, Nil)
repr:       Cons(   42, Nil)$\pause$
space:      2       1   1
\end{lstlisting}

\vfill

Opt-in: FFI concerns.
\end{frame}

\begin{frame}[fragile]{Constructor unboxing}
Our proposed extension.

\vfill

\begin{lstlisting}
type bignum =
| Small of int [@unboxed]
| Big of Gmp.t [@unboxed]
\end{lstlisting}

\vfill

Perf: 20\% time speedup on \texttt{bignum} micro-benchmark.

~\\

Other locality benefits for space-bound programs.
\end{frame}

\begin{frame}[fragile]{Forbidding confusion}

\begin{lstlisting}
type t =
  | Id of int [@unboxed]
  | Error of error_code [@unboxed]
and error_code = int$\pause$
/[Error: This declaration is invalid,
some [@unboxed] annotations introduce
overlapping representations.]/
\end{lstlisting}

\vfill

A static analysis at type-declaration time:
\begin{itemize}
\item abstract/approximate types into \emph{head shapes}
\item fail on non-disjoint shapes
\end{itemize}

\vfill

Precision tradeoff: performance, simplicity, portability.
\end{frame}

% \begin{frame}
% Constructors: runtime-checkable disjointness.\\
%
% (Note: This morality is language-independent.)
% \end{frame}

\begin{version}{\Long}
\section{Theory}
\frame{\sectionpage}
\end{version}

\begin{frame}{Computing the head shape?}
  How to compute the head shape of a type?
\end{frame}

\begin{frame}[fragile]{Computing the head shape?}
\begin{lstlisting}
type 'a tree = Node of ('a * 'a tree) seq [@unboxed]
and 'a seq = Nil | Next of (unit -> 'a * 'a seq) [@unboxed]
type foo = Foo of int tree [@unboxed] | ...
\end{lstlisting}

\vfill

\begin{lstlisting}
  shape(int tree)
= shape((int * int tree) seq)
= shape(Nil) + shape(... -> ...)
= {(Imm, 0)} + {(Block, Obj.closure_tag)}
\end{lstlisting}

\vfill\pause

Expanding a type definition is a $\beta$-reduction.

Call-by-name (head) normal form.

\vfill\pause

\begin{lstlisting}
let rec
  tree a = seq (prod a (tree a))
  seq a = nil + (arrow unit (prod a (seq a)))
  foo = tree int + ...
in tree int
\end{lstlisting}
\end{frame}

\begin{frame}[fragile]{Cycles}
\begin{lstlisting}
type t = U of u [@unboxed] | Bar
and u = T of t [@unboxed]

let rec
  t = u + bar
  u = t
in t
\end{lstlisting}

\vfill

Deciding termination?

\vfill

(STLC, just functions, \texttt{let rec}, first-order)
\end{frame}

\begin{frame}[fragile]{Attempt 1: rule out cycles statically}
  ``Statically'': without expanding definitions.

  \vfill

  (As done for type synonym/aliases.)

  \vfill

  Problem: too restrictive

  \vfill

  \begin{lstlisting}
    type 'a seq = ...

    type 'a tree = Node of ('a * 'a tree) seq [@unboxed]
  \end{lstlisting}
\end{frame}

\begin{frame}[fragile]{Attempt 2: prevent repetition of whole types}
  Block if the same type expression comes up again.

  \vfill

  \begin{lstlisting}
    type 'a bad = Loop of ('a * 'a) bad [@unboxed]
  \end{lstlisting}

  \vfill

  \begin{lstlisting}
    let rec
      bad a = bad (prod a a)
    in
    $\quad$ bad int
    $\rightarrow$ bad (prod int int)
    $\rightarrow$ bad (prod (prod int int) (prod int int))
    $\rightarrow$ ...
  \end{lstlisting}
\end{frame}

\begin{frame}[fragile]{Attempt 3: prevent repetition of head constructors}

  Abort if an expanded constructor comes again in head position.

  \vfill

  Problem: too restrictive

  \vfill

  \begin{lstlisting}[columns=flexible]
    let rec
      id a = a
      foo = id (id int)
    in
    $\quad      $ foo                         []
    $\rightarrow$ id (id int)                 [foo]
    $\rightarrow$ id int                      [foo, id]
    $\not\rightarrow$
  \end{lstlisting}
\end{frame}

\begin{frame}[fragile]{Solution: annotate (sub)expressions with expansion context}
  Track when subexpressions \emph{appeared} in the type,\\
  not how they came to head position.

  \vfill

  \begin{lstlisting}[columns=flexible]
    let rec
      id a = a
      delay a = id (id a)
      foo = delay int
    in
    $\quad      $ []foo
    $\rightarrow$ [foo](delay [foo]int)
    $\rightarrow$ [foo,delay](id ([foo,delay]id [foo]int)
    $\rightarrow$ [foo,delay](id [foo]int)
    $\rightarrow$ [foo]int
  \end{lstlisting}

  \vfill

  (Remark: similar to \texttt{cpp} termination control.)
\end{frame}

\begin{frame}
  Sound: ensures termination.

  Complete (in the first-order fragment):
    only rejects non-normalizing terms.

  \vfill

  Wait, is this problem decidable?
\end{frame}

\begin{frame}{Types-list to the rescue}
  \textcolor{blue}{[TYPES] Reference request: decidability of head normalization for a pure first-order calculus with recursion.}

  ~\\

  Excellent replies by

  ~~Pablo Barenbaum, Martin Lester, Gordon Plotkin, Sylvain Salvati.

  ~\\

  ``Recursion does not always help.''
  \citet*{higher-order-halting-problem}

  + literature on Higher-Order Model Checking

  \vfill\pause

  How to relate our first-order algorithm

  to existing higher-order algorithms?

  \vfill\pause

  \begin{center}
    Thanks! Questions?
  \end{center}
\end{frame}

\begin{frame}
\bibliography{long/unboxing}
\end{frame}
\end{document}
