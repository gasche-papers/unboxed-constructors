Nous proposons une implémentation d'une nouvelle fonctionnalité pour
OCaml, l'unboxing de constructeur. Elle permet d'élimineer certains
constructeurs de la représentation dynamique des valeurs quand cela ne
crée pas d'ambiguité entre différentes valeurs au même type. Nous
décrivons:

- l'analyse statique nécessaire pour accepter ou rejeter
  l'unboxing d'un constructeur,

- l'impact sur la compilation du filtrage de motif, et

- un cas d'usage préliminaire sur les grands entiers où la
  fonctionnalité améliore les performances de code OCaml idiomatique,
  éliminant le besoin d'écrire du code non-sûr.

Pour notre analyse statique, nous devons normaliser certaines
expressions de type, avec une relation de normalisation qui ne termine
pas nécessairement en présence de types mutuellement récursifs; nous
décrivons une analyse de terminaison qui garantit la normalisation
sans rejeter les déclarations de types qui nous intéressent.
